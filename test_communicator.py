##############################################################################
# | test_communicator file |                                                   #
# Author : Reuben Massaquoi & Afeefa Firdaus                                                 #
#Tests that the controller is sending commands via udp packets               #
##############################################################################

import unittest
import threading
import time
import controller
import communicator

class test_communicator(unittest.TestCase):

    test_IP = "127.0.0.1"
    conn_type = 0
    test_port = 9999
    reciever_communicator = communicator.communicator(test_port,test_IP,conn_type)
    sender_communicator = communicator.communicator(test_port,test_IP,conn_type)
 #   test_cmnd_com = communicator.communicator(9999,"127.0.0.1",0)

    
    def send(self):
        i = 100
        while True:
            i=i-1
            packet = "This is a Test."
            self.sender_communicator.send_packets(packet)
            time.sleep(.005)   
    def test_send_packets(self):
        sender = threading.Thread(target = self.send)
        sender.start()
        i = 10
        p = None
        
        while p is None:
            i=i-1
            
            p =  self.reciever_communicator.recieve_packets()
   
        self.assertEqual("This is a Test.",p)

if __name__ == '__main__':
    unittest.main()
    

