##############################################################################
# | gamepad file |                                                           #
# Author: Afeefa Firdaus & Brian Bailey                                      #
##############################################################################

#The following class allows the drone to be used from a joystick/gamepad. We have
#establish
# The joystick/gamepad should be taking commands ALL THE TIME.

import pygame


"""The gamepad object contains methods that allows the Drone to be controlled via Controller."""

class gamepad(object):
 
    joystick = None
    btn_press = False
    
   # empty constuctor.
    def __init__(self):
        pygame.init()
        pygame.joystick.init() #initialize joysticks
        
  # returns a list of input values.
    def get_inputs(self):
        
        self.btn_press = False
        input_list = [None,None,None,None,None,None]
        #index value for input_list                 
        i_index=0
        
        for event in pygame.event.get():# "If user did something"
            if event.type == pygame.QUIT:
                pygame.quit()
            if event.type == pygame.JOYBUTTONDOWN:
                self.btn_press=True

        #get the number of available joysticks and initializes them.
        joystick_count = pygame.joystick.get_count()
        for i in range(joystick_count):
            self.joystick = pygame.joystick.Joystick(i)
            self.joystick.init()
            #get the number of axes on gamepad & places values in input_list.
            axes = self.joystick.get_numaxes()
            for i in range (axes):
                axis = self.joystick.get_axis (i)
                input_list[i_index]=axis
                i_index = i_index + 1
                
        if self.btn_press:
            #places gamepad button values in input_list
            input_list[4] =  self.get_start_button()
            input_list[5] =  self.get_select_button()
            
        return input_list
    
    #get the button from gamepad and if it exists, then returns action.
    def get_start_button(self):
        if self.joystick.get_button(9):
            return self.joystick.get_button(9)
        
    def get_select_button(self):
        if self.joystick.get_button(8):
            return self.joystick.get_button(8)            

    




