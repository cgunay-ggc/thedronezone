##############################################################################
# | navdata file |                                                           #
# Author: Reuben Massaquoi                                                   #
##############################################################################
import communicator
import decoder
import threading
import queue
from multiprocessing import Process, Pipe
import time

"""The navdata_processor class is a class that wraps the functionality
    of the communicator class and the decoder class"""

#certain functions in this class will be running asynchronously from the main application but they will be tied together
#communications must first be established with the drones navigation port.
#the drone will then begin sending navigation data
#the navigation data will be collected at a rate of every 5ms
#the application will determine the rate at which said data will be decoded
#and relayed(possibly once a second)
#the navigation data will be funneled into a one way queue for the decoding subprocess
#the decoding subprocess will, through a child Pipe, return the decoded data in a preconfigured format
#In the main application a navdata retrieval thread will retrieve the necessary navdata that will be used to manage the application at run time

class navdata_processor(object):
    _running = None
    _navdata_communicator = None
   
    _input_pipe = None
    _output_pipe = None
       
    def __init__(self,nd_com):
        self._navdata_communicator = nd_com
        self._running = True
        self._input_pipe, self._output_pipe = Pipe()         

    def run(self,out_queue):
        #start decoding process
        p = Process(target = decoder.decode_navdata, name = "Decoding_Process",args = (self._output_pipe,))
        p.start()
        
         #start navdata retrieval thread
        _run_thread = threading.Thread(target = self.update,args = (self._input_pipe,out_queue))
        _run_thread.start()

    #recieve encoded data, and retrieves decoded data to be sent to main app
    def update(self,conn,out_queue):
        while self._running:
            p = self._navdata_communicator.recieve_packets(65535)
            conn.send(p)
            time.sleep(.05)
            out_queue.put(conn.recv())
    def shutdown(self):
        self._running = False
        self._input_pipe.send("shutdown")
            
 
        
