##############################################################################
# | command file |                                                           #
# Author: Reuben Massaquoi                                                   #
# Object for creating commands
##############################################################################


import struct
class command(object):
    
    _seq = 1
    
    
#The following method generates different commands that the AR Drone can interpret

#@param
    #arg1: "Command type value in the form of a string
        #exs: "REF","CONFIG","FTRIM", "CALIB",
    #arg2: "Command parameters"("Command Type" dependent) in the form of a list

    #In order to test this class you must provide:
        # A  string "Command Type" and a list of "Command parameter" values("Command Type" dependent)
    #@return: A string Command in the following format:
        #ex: "AT*REF=1,arg1"

    def generate_command(self,cmnd,param):
        command = None
        AT = "AT*"+cmnd+"="
        if cmnd == "REF":
            bit_field = 0
            #emergency takes priority as the first index in the param list.
            #Setting to True indicates that emergency mode should be activated.
            #Setting to False indicates that emergency mode should not be activated.
            if param[0]:
                bit_field = 290717952
            #takeoff and landing take a less priority than emergency as second index. Setting this index to True indicates takeoff mode
            #setting to false indicates landing mode.
            elif param[1]:
                bit_field = 290718208
            #landing
            else:
                bit_field = 290717696
            
            command = AT + str(self._seq) + "," + str(bit_field) + "\r"
            
        else:
            args =""
            if param:
                for item in param:
                    t = type(item)
                    if t is str:
                        args = args + ",\"" + item + "\""
                    elif t is float:
                        args = args + "," +  str(struct.unpack("i",struct.pack("f",item))[0])
                    elif t is int:
                        args = args + "," +  str(item)
                        
                command = AT + str(self._seq) + args  + "\r"
            else:
                command = AT + str(self._seq) + "\r"
                

        self._seq+=1
        return command;
    
    def reset_seq(self):
        self._seq = 0
    def get_seq(self):
        return self._seq

  
