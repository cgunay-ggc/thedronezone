##############################################################################
# | test_gamepad file |                                                               #
# Author : Reuben Massaquoi & Afeefa Firdaus & Brian Bailey                                                 #                                                  #
##############################################################################
import unittest
import gamepad

class test_gamepad(unittest.TestCase):
    gp = None

    def test_Left_analog_axis_1(self):
        gp = gamepad.gamepad()
        self.assertIsNotNone(gp.get_inputs()[2])

    def test_Left_analog_axis_2(self):
        gp = gamepad.gamepad()
        self.assertIsNotNone(gp.get_inputs()[3])
        
        
        
    
if __name__ == '__main__':
    unittest.main()
    
