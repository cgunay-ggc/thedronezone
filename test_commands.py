##############################################################################
# | controller file |                                                        #
# Author(s): Reuben Massaquoi & Brian Bailey                                    #
##############################################################################


import unittest
import command

class test_commands(unittest.TestCase):
    
    def test_ref_command_emergency_mode(self):
        command_object = command.command()  
        cmnd = "REF"
        param = [True,False]
        test_command = "AT*REF=1,290717952\r"
        c = command_object.generate_command(cmnd,param)
        self.assertEqual(c,test_command)
    def test_ref_command_takeoff_mode(self):
        command_object = command.command()
        cmnd = "REF"
        param = [False,True]
        test_command = "AT*REF=1,290718208\r"
        c = command_object.generate_command(cmnd,param)
        self.assertEqual(c,test_command)
    def test_ref_command_landing_mode(self):
        command_object = command.command()
        cmnd = "REF"
        param = [False,False]
        test_command = "AT*REF=1,290717696\r"
        c = command_object.generate_command(cmnd,param)
        self.assertEqual(c,test_command)
    def test_reset__waatchdog_mode_command(self):
        command_object = command.command()
        cmnd = "PCMD"
        param = []
        test_command = "AT*PCMD=1\r"
        c = command_object.generate_command(cmnd,param)
        self.assertEqual(c,test_command)
    def test_seq_numnber(self):
        command_object = command.command()  
        cmnd = "REF"
        param = [True,False]
        test_command = "AT*REF=1,290717952\r"
        c = command_object.generate_command(cmnd,param)
        c = command_object.generate_command(cmnd,param)
        seq = 2
        self.assertEqual(command_object.get_seq(),seq)
    def test_config_ids(self):
        command_object = command.command()  
        cmnd = "CONFIG_IDS"
        param = ["pr]
        test_command = "AT*REF=1,290717952\r"
        c = command_object.generate_command(cmnd,param)
        seq = 2
        self.assertEqual(command_object.get_seq(),seq)
    
if __name__ == '__main__':
    unittest.main()
    
