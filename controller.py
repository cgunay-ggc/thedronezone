##############################################################################
# | controller file |                                                        #
# Author: Reuben Massaquoi                                                   #
##############################################################################


#in order to control the drone we must establish a connection on it's command port 5556.
#we must then initiate a task of reseting the command watchdog every 50 ms if and only 
#if the controller does not send other commands in that time frame.
#we must also set in place a fail safe to reestablish full communications if 2 seconds pass with no commands being sent.
#upon request of a command they will be placed in a queue.
#The controller will run a task that will periodically check the queue for outgoing commands
#and will send those commands every 30ms


import communicator
import queue
import config_data
import threading
import time


"""The controller object contains a set of methods that wraps
the functionality of the command object and the communicator object
in order to manipulate the AR 2.0 Drone"""

class controller(object):

    _command_object = None
    _command_communicator = None
    _msg_queue = None
    _session_id = ""
    _profile_id = ""
    _application_id = ""
    _watchdog_timer = None
    _failSafe = None
    _run_thread = None
    _running = True

    

    def __init__(self,cmnd_com,cmnd_obj):
        
        self._command_object = cmnd_obj
        self._command_communicator = cmnd_com
        self._msg_queue = queue.Queue(5)
        self._session_id = config_data.session_id
        self._profile_id = config_data.profile_id
        self._application_id = config_data.application_id
        

    #sends configuration identification data
    def send_config_ids(self):
        param = [self._session_id,self._profile_id,self._application_id]
        p = self._command_object.generate_command("CONFIG_IDS",param)
        self._msg_queue.put(p)
        

    def send_ack_command(self):
        p = self._command_object.generate_command("CTRL",[])
        self._command_communicator.send_packets(p)
        self._msg_queue.put(p)
        
    #sends a command to reset the command watch_dog of the drone in order for it to repond to other commands
    def reset_watchdog(self):
        p = self._command_object.generate_command("COMWDG",[])
        #self._command_communicator.send_packets(c)
        self._msg_queue.put(p)
        #print("reseting watchdog")

    #Must be sent before flying
    #Must ONLY be sent when on the ground
    def set_ftrim(self):
        p = self._command_object.generate_command("FTRIM",[])
        self._msg_queue.put(p)
        
    #Must be sent when flying
    #Must ONLY be sent when flying
    def calibrate_magnometer(self):
        p = self._command_object.generate_command("CALIB",[])
        self._msg_queue.put(p)
        
        
    #takes two arguments, an configurable option and the value for said option and sends a configuration command
    def configure(self,option,value):
        param = [option,value]
        p =  self._command_object.generate_command("CONFIG",param)
        self._msg_queue.put(p)
    def config_profile(self):
        self.configure("custom:profile_id",self._profile_id)
    def config_session(self):
        self.configure("custom:session_id",self._session_id)
    def config_application(self):
        self.configure("custom:application_id",self._application_id)

    
    def activate_demo_mode(self):
        self.send_config_ids()
        self.configure("general:navdata_demo","TRUE")
    def activate_full_mode(self):
        self.send_config_ids()
        self.configure("general:navdata_demo","FALSE")


    #Controls the drones flight state/mode. Either emergency, takeoff or landing
    #See command module to understand params
    #Default args are both False, indicating landing mode/state.
    def set_flight_mode(self,to_emergency = False,to_takeoff = False):
        param = [to_emergency,to_takeoff]
        p =  self._command_object.generate_command("REF",param)
        self._msg_queue.put(p)

    #Moves drone using basic precision
    #arg values must be floats between -1 and 1
    def move_drone(self,roll,pitch,gaz,yaw):
        param = [1,float(roll),float(pitch),float(gaz),float(yaw)]
        p =  self._command_object.generate_command("PCMD",param)
        self._msg_queue.put(p)
        

    #Establishes connection to drone and runs concurrent thread for sending commands every 30ms 
    def run(self):
        #self.connect()
        #must be sent every 50ms if no other commands are being sent to the drone
        self.set_ftrim()
        self._watchdog_timer = threading.Timer(.5,self.reset_watchdog)
        self._watchdog_timer.start()
        self._run_thread = threading.Thread(target = self.update)
        self._run_thread.start()
        


    
    def update(self):
        while self._running:
            if not self._msg_queue.empty():
                p = self._msg_queue.get()
               
                self._command_communicator.send_packets(p)
                #reset watchdog timer after sending command in msg_queue
                self._watchdog_timer.cancel()
                self._watchdog_timer = threading.Timer(.1,self.reset_watchdog)
                self._watchdog_timer.start()

            time.sleep(.03)
            
            
    def shutdown(self):
        self._running = False
            #print(self._command_communicator.recieve_packets())

        

    
