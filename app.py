##############################################################################
# | app file |                                                               #
# Author : Reuben Massaquoi & Afeefa Firdaus                                 #          
##############################################################################
import controller
import command
import navdata
import communicator
import gamepad
import queue
import threading
import time
import socket
import csv
from multiprocessing import Process, Pipe
#from sys import argv, exit
#from collections import OrderedDict


#class for interfacing with and manipulating the drone

class app(object):
    drone_IP = "192.168.1.1"
    command_port = 5556
    navdata_port = 5554
    video_port = 5555
    control_port = 5559
    _controller = None
    _cmnd_com = None
    _cmnd_obj = None
    _navdata_processor = None
    _nd_com = None
    _gamepad = gamepad.gamepad()
    _data_queue = queue.Queue(10)
    _navdata_timer = None
    _demodata = None
    _statedata = None
    _timedata = None
    _pwmdata = None
    _running = False
    _takeoff_lock = False
    _log_lock = False
    _logging = False
    _log_rate = 0
    _log_headers = None
    navdata = None
    network_connection = False
    data_connection = False

    def __init__(self):
        
        #Initialize socket communication objects
        self._cmnd_com = communicator.communicator( self.command_port, self.drone_IP,0)
        self._cmnd_obj = command.command()
        self._controller = controller.controller( self._cmnd_com, self._cmnd_obj)
        
        self._nd_com = communicator.communicator( self.navdata_port, self.drone_IP,0)
        self._navdata_processor = navdata.navdata_processor( self._nd_com)
        
        self._running = True
        self._log_rate =1
        self._log_headers = ['','','','','','','']

    #Function establishes initial communications with drone
    def connect(self):
        if self._running:
            try:
                s = socket.socket()
                s.connect((self.drone_IP,5559))
                s.close()
            except:
                print("drone is not online")
            #send initial packets of some bits to initiate communications with command and nav port
            self._nd_com.send_packets("\x01\x00\x00\x00")
            self._cmnd_com.send_packets("\r")
            time.sleep(.1)
            self._controller.send_config_ids()
            self._controller.config_profile()
            time.sleep(.1)
            self._controller.send_config_ids()
            self._controller.config_session()
            time.sleep(.1)
            self._controller.send_config_ids()
            self._controller.config_application()
            time.sleep(.1)
            p =self._cmnd_obj.generate_command("PMODE",[])
            self._cmnd_com.send_packets(p)
            time.sleep(.03)
            p =self._cmnd_obj.generate_command("COMWDG",[])
            self._cmnd_com.send_packets(p)
            self.network_connection = True

    #returns True if drone is fully connected to application channels
    def connected(self):
        if  self.network_connection and self.data_connection:
            return True
        else:
            return False
  

    # a function that checks the current fly mask of the drone and sends the respective takeoff or landing command
    def set_flight_state(self):
        self._takeoff_lock = True
        try:
            #if self._statedata[15]==0:
             #   print("The drones battery is too low to fly! Please recharge!")
              #  self._takeoff_lock = False
            #else:
            #checks landed state
            if self._statedata[0]==0:
                self._controller.set_ftrim()
                time.sleep(.5)
                self._controller.set_ftrim()
                time.sleep(.5)
                self._controller.set_ftrim()
                time.sleep(.5)
                #while drone is not flying
                while not self._statedata[0] ==1:
                    self._controller.set_flight_mode(False, True)
                    print("taking off")
                    time.sleep(.5)
                
            #checks flying state
            elif self._statedata[0]==1:
                #while drone is not landing
                while not self._statedata[0]==0:
                    self._controller.set_flight_mode(False,False)
                    print("Landing")
                    time.sleep(.5)

            self._takeoff_lock = False
        except:
            self._takeoff_lock = False
            print("fly state error")


    def _initiate_demodata(self):
        init = True
        while init:
            try:
                while self._statedata[11]!=0:
                    self._controller.send_config_ids()
                    self._controller.activate_demo_mode()
                    
                    if self._statedata[11]==1:
                        while self._statedata[6]!=1:
                            time.sleep(.01)
                            self._controller.send_ack_command()
                    else :
                         print("There are no options packages")
                    time.sleep(.01)
                print("WE GOT DEMO DATA")
                init = False
            except:
                print("could not initiate demo data")
            time.sleep(.01)
           
    #function for initiating full data mode(necessary for controlling drone)                 
    def _initiate_fulldata(self):
        init = True
        while self._running:
            try:
                while self._statedata[11]!=0 or self._statedata[10]!=0:
                    #print("CHECKING STATE BIT")
                    self._controller.send_config_ids()
                    self._controller.activate_full_mode()
                    if self._statedata[11]==1:
                       while self._statedata[6]!=1:
                           time.sleep(.01)
                           self._controller.send_ack_command()
                    time.sleep(.01)
                print("WE GOT FULL DATA")
                self.data_connection = True
                break
            except:
                print("failed to initiate full data")
            time.sleep(.01)
            
                 
             
    def retrieve_navdata(self):
        while not  self._data_queue.empty():
            return self._data_queue.get()
                
                    
    def _calibrate_drone(self):
        # checks if magnometer needs to be calibrated and sends recalibration immediately
        #checks if drone is flying
        if self._statedata[0] == 1:
            if self._statedata[18] == 1:
                self._controller.calibrate_magnometer()

            
    def handle_input(self):
        #set the initial input values to None.  
        input_takeoff = None
        input_emergency = None
        input_yaw_rotate = None #rotation 2
        input_gaz = None #vertical speed 3
        input_roll_direction = None #left/right 0
        input_pitch_direction = None #front/back 1
     
        inputs =  self._gamepad.get_inputs()
        
        #set the rotation/direction based on input values from the gamepad
        #due to gamepad error we check for low value range of analog sticks
        if inputs[0]<=0 and inputs[0]>=-0.0001:
            input_roll_direction = 0
        else:
            input_roll_direction = inputs[0]
            
        if inputs[1]<=0 and inputs[1]>=-0.0001:
            input_pitch_direction = 0
        else:
            input_pitch_direction = inputs[1]
            
        if inputs[2]<=0 and inputs[2]>=-0.0001:
            input_yaw_rotate = 0
        else:
            input_yaw_rotate = inputs[2]
            
        if inputs[3]<=0 and inputs[3]>=-0.0001:
            input_gaz = 0
        else:
             input_gaz = inputs[3]
       
        input_takeoff = inputs[4]
        input_emergency = inputs[5]

        #On the press of the takeoff button
        if input_takeoff:
            if not self._takeoff_lock:
                f = threading.Thread(target =self.set_flight_state )
                f.start()
        #On the Press of the emergency button
        if input_emergency:
            self._controller.set_flight_mode(True, False)
            print("Emergency Mode")
       
        if input_yaw_rotate > 0 or input_yaw_rotate <= -0.0001 or input_gaz >0 or input_gaz <=-0.0001 or input_roll_direction >0 or input_roll_direction <=-0.0001 or  input_pitch_direction >0 or  input_pitch_direction <=-0.0001:
            self._controller.move_drone(input_roll_direction, input_pitch_direction,input_gaz*(-1),input_yaw_rotate)               
        else:
            #resets velocity variables
            self._controller.move_drone(0,0,0,0)


    #generates a csv File of data based off of Interface configuration
    def gen_csv(self):
        with open('data_log.csv', 'w', newline='') as csvfile:
            writer = csv.writer(csvfile, delimiter=' ',
                                    quotechar='|', quoting=csv.QUOTE_MINIMAL)
            t = 0.0
            last_time = 0.0
            writer.writerow(['time_stamp(s),'] + [self._log_headers[0]] + [self._log_headers[1]] + [self._log_headers[2]] + [self._log_headers[3]] + [self._log_headers[4]] + [self._log_headers[5]] + [self._log_headers[6]])
            while self._logging:
                
                try:
                    log_list=[]
                    t = self._timedata[0]
                    st =str(t)+","
                    log_list.append(st)
                    if not self._log_headers[0] == '':
                        x = str(self._demodata[4][0])+","
                        log_list.append(x)
                    if not self._log_headers[1] == '':
                        y = str(self._demodata[4][1])+","
                        log_list.append(y)
                    if not self._log_headers[2] == '':
                        z = str(self._demodata[4][2])+","
                        log_list.append(z)
                    if not self._log_headers[3] == '':
                        p = str(self._demodata[2][0])+","
                        log_list.append(p)
                    if not self._log_headers[4] == '':
                        r = str(self._demodata[2][1])+","
                        log_list.append(r)
                    if not self._log_headers[5] == '':
                        ya = str(self._demodata[2][2])+","
                        log_list.append(ya)
                    if not self._log_headers[6] == '':
                        a = str(self._demodata[3])+","
                        log_list.append(a)
                    if t-last_time>= self._log_rate:
                        last_time = t
                        writer.writerow(log_list)
                except:
                    print("bad data")
                time.sleep(.01)
            csvfile.close()
        print ("Your file is ready.")

    def start_log(self):
        if self._statedata[0] == 1:
            print ("logging")
            self._log_lock = True
            self._logging = True
            gen_csv_thread = threading.Thread(target = self.gen_csv)
            gen_csv_thread.start()
            
       
    def end_log(self):
        if self._statedata[0] == 0:     
           self._log_lock = False
           self._logging = False
           print("ending log")
        

    def log_data(self):
        if not self._log_lock:
            self.start_log()
        else:
            self.end_log()
                    


    def run(self):
        connect_thread = threading.Thread(target = self.connect)
        connect_thread.start()
      
        #Start NavData_processor
        self._navdata_processor.run( self._data_queue)    

        #start Controller
        self._controller.run()


        data_thread = threading.Thread(target = self._initiate_fulldata)
        data_thread.start()
       
        #Main Application Loop
        while self._running:
            self.navdata = self.retrieve_navdata()

            if self.network_connection:
                try:
                    #print(navdata)
                    self._statedata = self.navdata["state"]
                except:
                    print("There is no State data")
                
            if self.data_connection:
                try:
                    self._demodata = self.navdata["demo"]
                except:
                    print("There is no Demo data")
                try:
                    self._timedata = self.navdata["time"]
                except:
                    print("There is no Time data")
                try:
                    self._calibrate_drone()
                except:
                    print("cannot calibrate atm")
                try:
                    self.log_data()
                except:
                    print("cannot log")
                self.handle_input()
            time.sleep(.060)

    #immedicately lands drone and prepares to close application
    def shutdown(self):
        try:
            while not self._statedata[0]==0:
                self.set_flight_state()
        except:
            print("Failed to land drone")
        self._running = False
        self._takeoff_lock = True
        self._controller.shutdown()
        self._navdata_processor.shutdown()
        print("shutting down application. GOODBYE")
        
      
            
            

if __name__ == '__main__':
    
    controller_app = app()
    r = threading.Thread(target = controller_app.run)
    r.start()
    
