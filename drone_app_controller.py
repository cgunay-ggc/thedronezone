##############################################################################
# | drone_app_controller file |                                              #
# Author : Reuben Massaquoi & Afeefa Firdaus  & Brian Bailey                 #          
##############################################################################
import app
from tkinter import *
import threading
import time
import multiprocessing


#this class contains GUI implementations using the tkinter module
class drone_app_controller(object):

    _running = True

    #ctrl variables for storing checkbutton info for determining log data and rate/frequency
    x_ctrl =None 
    y_ctrl = None
    z_ctrl = None
    yaw_ctrl = None
    pitch_ctrl = None
    roll_ctrl = None
    altitude_ctrl = None
    log_rate_ctrl = None
    c = None

    #ctrl variables for storing  data feed values
    xval_ctrl = None
    yval_ctrl = None
    zval_ctrl = None
    alval_ctrl = None

    #ctrl vairable for storing battery level value
    batVal_ctrl = None
    batVal = None
    

    _app  = None
    window = None
  




    def __init__(self):
        self.window = Tk()
        self.x_ctrl = IntVar() 
        self.y_ctrl = IntVar()
        self.z_ctrl = IntVar()
        self.yaw_ctrl = IntVar()
        self.pitch_ctrl = IntVar()
        self.roll_ctrl = IntVar()
        self.altitude_ctrl = IntVar()
        self.log_rate_ctrl = DoubleVar()
        self.xval_ctrl = StringVar()
        self.yval_ctrl = StringVar()
        self.zval_ctrl = StringVar()
        self.alval_ctrl = StringVar()
        self.batVal_ctrl = StringVar()
        
        self._app = app.app()
        r = threading.Thread(target = self._app.run)
        r.start()

        r1 = threading.Thread(target = self.update, daemon = True)
        r1.start()
        
    def set_log_rate(self,val):
        self._app._log_rate = val
    #configures data logging values 
    def set_log_data(self,x_v,y_v,z_v,y,p,r,a,rate):
        self._app._log_headers = []
        if x_v.get() == 0:
            self._app._log_headers.append('')
        else:
            self._app._log_headers.append("x_velocity(mm/s),")
        if y_v.get() == 0:
            self._app._log_headers.append('')
        else:
            self._app._log_headers.append("y_velocity(mm/s),")
        if z_v.get() == 0:
            self._app._log_headers.append('')
        else:
            self._app._log_headers.append("z_velocity(mm/s),")
        if p.get() == 0:
            self._app._log_headers.append('')
        else:
            self._app._log_headers.append("pitch(deg),")
        if r.get() == 0:
            self._app._log_headers.append('')
        else:
            self._app._log_headers.append("roll(deg),")
        if y.get() == 0:
            self._app._log_headers.append('')
        else:
            self._app._log_headers.append("yaw(deg),")
        if a.get() == 0:
            self._app._log_headers.append('')
        else:
            self._app._log_headers.append("altitude(cm),")

        self.set_log_rate(rate.get())

        print("Changes have been applied")

    #function for updating data feed values
    def update(self):
        while self._running:
            if self._app.connected():
                nd = self._app.navdata
                if nd:
                    try:
                        x = str(((int)(nd["demo"][4][0]*1000))/1000)+'mm/s'
                        y =str(((int)(nd["demo"][4][1]*1000))/1000)+'mm/s'
                        z = str(((int)(nd["demo"][4][2]*1000))/1000)+'mm/s'
                        a = str(nd["demo"][3])+'cm'
                        b = nd["demo"][1]
                        self.xval_ctrl.set(x)
                        self.yval_ctrl.set(y)
                        self.zval_ctrl.set(z)
                        self.alval_ctrl.set(a)
                        self.batVal_ctrl.set( str(b)+'%')
                        self.c = None
                        if b >40:
                            self.c = "#008000"
                        elif b <=40 and b >=25:
                            self.c = "#ff8000"
                        else:
                            self.c = "#ff0404"
                        self.batVal.configure(background=self.c)
                    except:
                        print("There was a problem updating the display feed")
                time.sleep(.5)
        print("shutting down data feed")


    def shutdown(self):
        self._running = False
        self._app.shutdown()
        self.window.destroy()
        
    def vp_start_gui(self):
        '''This function configures and populates the toplevel window.
            self.window is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font10 = "-family {Segoe UI Semibold} -size 11 -weight bold "  \
            "-slant roman -underline 0 -overstrike 0"
        font12 = "-family {Segoe UI Semibold} -size 18 -weight bold "  \
            "-slant roman -underline 0 -overstrike 0"
        font14 = "-family {Segoe UI Semibold} -size 24 -weight bold "  \
            "-slant roman -underline 0 -overstrike 0"
        font15 = "-family {Segoe UI} -size 13 -weight normal -slant "  \
            "roman -underline 0 -overstrike 0"
        font16 = "-family {Segoe UI} -size 14 -weight normal -slant "  \
            "roman -underline 0 -overstrike 0"
        font17 = "-family {Segoe UI Semibold} -size 16 -weight bold "  \
            "-slant roman -underline 0 -overstrike 0"

        self.window.geometry("1403x747+72+27")
        self.window.title("Main")
        self.window.configure(background="#c0c0c0")
        self.window.configure(highlightbackground="#d9d9d9")
        self.window.configure(highlightcolor="black")
        self.window.resizable(width=False, height=False)



        self.Title = Label( self.window)
        self.Title.place(relx=0.0, rely=0.0, height=91, width=1404)
        self.Title.configure(activebackground="#f9f9f9")
        self.Title.configure(activeforeground="black")
        self.Title.configure(background="#008000")
        self.Title.configure(disabledforeground="#a3a3a3")
        self.Title.configure(font=font14)
        self.Title.configure(foreground="#ffffff")
        self.Title.configure(highlightbackground="#d9d9d9")
        self.Title.configure(highlightcolor="black")
        self.Title.configure(text='''AR Parrot 2.0 Drone Controller V1.0''')
        self.Title.configure(width=1404)
        img = PhotoImage(file = "instructions.png")
        self.Instructions = Canvas(self.window)
        self.Instructions.place(relx=0.24, rely=0.17, relheight=0.71
                , relwidth=0.48)
        self.Instructions.configure(background="white")
        self.Instructions.configure(borderwidth="2")
        self.Instructions.configure(highlightbackground="#d9d9d9")
        self.Instructions.configure(highlightcolor="black")
        self.Instructions.configure(insertbackground="black")
        self.Instructions.configure(relief=RIDGE)
        self.Instructions.configure(selectbackground="#c4c4c4")
        self.Instructions.configure(selectforeground="black")
        self.Instructions.create_image(336,270,image =img)
        self.Instructions.configure(width=676)

        self.Frame1 = Frame(self.window)
        self.Frame1.place(relx=0.76, rely=0.33, relheight=0.37, relwidth=0.2)
        self.Frame1.configure(relief=GROOVE)
        self.Frame1.configure(borderwidth="2")
        self.Frame1.configure(relief=GROOVE)
        self.Frame1.configure(background="#000000")
        self.Frame1.configure(highlightbackground="#d9d9d9")
        self.Frame1.configure(highlightcolor="black")
        self.Frame1.configure(width=285)

        self.Label3 = Label(self.Frame1)
        self.Label3.place(relx=0.04, rely=0.04, height=26, width=91)
        self.Label3.configure(activebackground="#f9f9f9")
        self.Label3.configure(activeforeground="black")
        self.Label3.configure(background="#000000")
        self.Label3.configure(disabledforeground="#a3a3a3")
        self.Label3.configure(font=font10)
        self.Label3.configure(foreground="#ffffff")
        self.Label3.configure(highlightbackground="#d9d9d9")
        self.Label3.configure(highlightcolor="black")
        self.Label3.configure(text='''X_VELOCITY:''')

        self.Label4 = Label(self.Frame1)
        self.Label4.place(relx=0.04, rely=0.23, height=26, width=87)
        self.Label4.configure(activebackground="#f9f9f9")
        self.Label4.configure(activeforeground="black")
        self.Label4.configure(background="#000000")
        self.Label4.configure(disabledforeground="#a3a3a3")
        self.Label4.configure(font=font10)
        self.Label4.configure(foreground="#ffffff")
        self.Label4.configure(highlightbackground="#d9d9d9")
        self.Label4.configure(highlightcolor="black")
        self.Label4.configure(text='''Y_VELOCITY:''')

        self.Label5 = Label(self.Frame1)
        self.Label5.place(relx=0.04, rely=0.41, height=26, width=87)
        self.Label5.configure(activebackground="#f9f9f9")
        self.Label5.configure(activeforeground="black")
        self.Label5.configure(background="#000000")
        self.Label5.configure(disabledforeground="#a3a3a3")
        self.Label5.configure(font=font10)
        self.Label5.configure(foreground="#ffffff")
        self.Label5.configure(highlightbackground="#d9d9d9")
        self.Label5.configure(highlightcolor="black")
        self.Label5.configure(text='''Z_VELOCITY:''')

        self.Label6 = Label(self.Frame1)
        self.Label6.place(relx=0.04, rely=0.59, height=26, width=87)
        self.Label6.configure(activebackground="#f9f9f9")
        self.Label6.configure(activeforeground="black")
        self.Label6.configure(background="#000000")
        self.Label6.configure(disabledforeground="#a3a3a3")
        self.Label6.configure(font=font10)
        self.Label6.configure(foreground="#ffffff")
        self.Label6.configure(highlightbackground="#d9d9d9")
        self.Label6.configure(highlightcolor="black")
        self.Label6.configure(text='''ALTITUDE :''')

        self.Label7 = Label(self.Frame1)
        self.Label7.place(relx=1.89, rely=1.52, height=26, width=87)
        self.Label7.configure(activebackground="#f9f9f9")
        self.Label7.configure(activeforeground="black")
        self.Label7.configure(background="#000000")
        self.Label7.configure(disabledforeground="#a3a3a3")
        self.Label7.configure(font=font10)
        self.Label7.configure(foreground="#ffffff")
        self.Label7.configure(highlightbackground="#d9d9d9")
        self.Label7.configure(highlightcolor="black")
        self.Label7.configure(text='''X_VELOCITY''')

   

        self.xvel_value = Label(self.Frame1)
        self.xvel_value.place(relx=0.42, rely=0.04, height=21, width=100)
        self.xvel_value.configure(background="#000000")
        self.xvel_value.configure(disabledforeground="#a3a3a3")
        self.xvel_value.configure(font=font10)
        self.xvel_value.configure(foreground="#ffffff")
        self.xvel_value.configure(text='''0.0''')
        self.xvel_value.configure(textvariable=self.xval_ctrl)
        self.xvel_value.configure(width=64)

        self.yvel_value = Label(self.Frame1)
        self.yvel_value.place(relx=0.4, rely=0.23, height=21, width=100)
        self.yvel_value.configure(activebackground="#f9f9f9")
        self.yvel_value.configure(activeforeground="black")
        self.yvel_value.configure(background="#000000")
        self.yvel_value.configure(disabledforeground="#a3a3a3")
        self.yvel_value.configure(font=font10)
        self.yvel_value.configure(foreground="#ffffff")
        self.yvel_value.configure(highlightbackground="#d9d9d9")
        self.yvel_value.configure(highlightcolor="black")
        self.yvel_value.configure(text='''0.0''')
        self.yvel_value.configure(textvariable=self.yval_ctrl)

        self.zvel_value = Label(self.Frame1)
        self.zvel_value.place(relx=0.4, rely=0.41, height=21, width=100)
        self.zvel_value.configure(activebackground="#f9f9f9")
        self.zvel_value.configure(activeforeground="black")
        self.zvel_value.configure(background="#000000")
        self.zvel_value.configure(disabledforeground="#a3a3a3")
        self.zvel_value.configure(font=font10)
        self.zvel_value.configure(foreground="#ffffff")
        self.zvel_value.configure(highlightbackground="#d9d9d9")
        self.zvel_value.configure(highlightcolor="black")
        self.zvel_value.configure(text='''0.0''')
        self.zvel_value.configure(textvariable=self.zval_ctrl)

        self.altitudevel_value = Label(self.Frame1)
        self.altitudevel_value.place(relx=0.4, rely=0.59, height=21, width=100)
        self.altitudevel_value.configure(activebackground="#f9f9f9")
        self.altitudevel_value.configure(activeforeground="black")
        self.altitudevel_value.configure(background="#000000")
        self.altitudevel_value.configure(disabledforeground="#a3a3a3")
        self.altitudevel_value.configure(font=font10)
        self.altitudevel_value.configure(foreground="#ffffff")
        self.altitudevel_value.configure(highlightbackground="#d9d9d9")
        self.altitudevel_value.configure(highlightcolor="black")
        self.altitudevel_value.configure(text='''0.0''')
        self.altitudevel_value.configure(textvariable=self.alval_ctrl)

        self.DeataFeed = Label( self.window)
        self.DeataFeed.place(relx=0.73, rely=0.22, height=61, width=347)
        self.DeataFeed.configure(activebackground="#f9f9f9")
        self.DeataFeed.configure(activeforeground="black")
        self.DeataFeed.configure(background="#008000")
        self.DeataFeed.configure(disabledforeground="#a3a3a3")
        self.DeataFeed.configure(font=font12)
        self.DeataFeed.configure(foreground="#ffffff")
        self.DeataFeed.configure(highlightbackground="#d9d9d9")
        self.DeataFeed.configure(highlightcolor="black")
        self.DeataFeed.configure(text='''Data feed''')
        self.DeataFeed.configure(width=347)

        self.menubar = Menu(self.window,font="TkMenuFont",bg=_bgcolor,fg=_fgcolor)
        self.window.configure(menu = self.menubar)



        self.Frame2 = Frame( self.window)
        self.Frame2.place(relx=0.02, rely=0.33, relheight=0.48, relwidth=0.17)
        self.Frame2.configure(relief=GROOVE)
        self.Frame2.configure(borderwidth="2")
        self.Frame2.configure(relief=GROOVE)
        self.Frame2.configure(background="#d9d9d9")
        self.Frame2.configure(highlightbackground="#d9d9d9")
        self.Frame2.configure(highlightcolor="black")
        self.Frame2.configure(width=245)

        self.x_vel1 = Checkbutton(self.Frame2)
        self.x_vel1.place(relx=0.06, rely=0.12, relheight=0.13, relwidth=0.41)
        self.x_vel1.configure(activebackground="#d9d9d9")
        self.x_vel1.configure(activeforeground="#000000")
        self.x_vel1.configure(background="#d9d9d9")
        self.x_vel1.configure(disabledforeground="#a3a3a3")
        self.x_vel1.configure(font=font16)
        self.x_vel1.configure(foreground="#000000")
        self.x_vel1.configure(highlightbackground="#d9d9d9")
        self.x_vel1.configure(highlightcolor="black")
        self.x_vel1.configure(justify=LEFT)
        self.x_vel1.configure(text='''y_velocity''')
        self.x_vel1.configure(variable=self.y_ctrl)

        self.x_vel2 = Checkbutton(self.Frame2)
        self.x_vel2.place(relx=0.04, rely=0.24, relheight=0.07, relwidth=0.45)
        self.x_vel2.configure(activebackground="#d9d9d9")
        self.x_vel2.configure(activeforeground="#000000")
        self.x_vel2.configure(background="#d9d9d9")
        self.x_vel2.configure(disabledforeground="#a3a3a3")
        self.x_vel2.configure(font=font16)
        self.x_vel2.configure(foreground="#000000")
        self.x_vel2.configure(highlightbackground="#d9d9d9")
        self.x_vel2.configure(highlightcolor="black")
        self.x_vel2.configure(justify=LEFT)
        self.x_vel2.configure(text='''z_velocity''')
        self.x_vel2.configure(variable=self.z_ctrl)
        self.x_vel2.configure(width=111)

        self.x_vel3 = Checkbutton(self.Frame2)
        self.x_vel3.place(relx=0.0, rely=0.06, relheight=0.07, relwidth=0.53)
        self.x_vel3.configure(activebackground="#d9d9d9")
        self.x_vel3.configure(activeforeground="#000000")
        self.x_vel3.configure(background="#d9d9d9")
        self.x_vel3.configure(disabledforeground="#a3a3a3")
        self.x_vel3.configure(font=font16)
        self.x_vel3.configure(foreground="#000000")
        self.x_vel3.configure(highlightbackground="#d9d9d9")
        self.x_vel3.configure(highlightcolor="black")
        self.x_vel3.configure(justify=LEFT)
        self.x_vel3.configure(text='''x_velocity''')
        self.x_vel3.configure(variable=self.x_ctrl)
        self.x_vel3.configure(width=131)

        self.Checkbutton7 = Checkbutton(self.Frame2)
        self.Checkbutton7.place(relx=0.55, rely=0.15, relheight=0.07
                , relwidth=0.31)
        self.Checkbutton7.configure(activebackground="#d9d9d9")
        self.Checkbutton7.configure(activeforeground="#000000")
        self.Checkbutton7.configure(background="#d9d9d9")
        self.Checkbutton7.configure(disabledforeground="#a3a3a3")
        self.Checkbutton7.configure(font=font16)
        self.Checkbutton7.configure(foreground="#000000")
        self.Checkbutton7.configure(highlightbackground="#d9d9d9")
        self.Checkbutton7.configure(highlightcolor="black")
        self.Checkbutton7.configure(justify=LEFT)
        self.Checkbutton7.configure(text='''Pitch''')
        self.Checkbutton7.configure(variable=self.pitch_ctrl)

        self.Checkbutton8 = Checkbutton(self.Frame2)
        self.Checkbutton8.place(relx=0.53, rely=0.07, relheight=0.07
                , relwidth=0.33)
        self.Checkbutton8.configure(activebackground="#d9d9d9")
        self.Checkbutton8.configure(activeforeground="#000000")
        self.Checkbutton8.configure(background="#d9d9d9")
        self.Checkbutton8.configure(disabledforeground="#a3a3a3")
        self.Checkbutton8.configure(font=font15)
        self.Checkbutton8.configure(foreground="#000000")
        self.Checkbutton8.configure(highlightbackground="#d9d9d9")
        self.Checkbutton8.configure(highlightcolor="black")
        self.Checkbutton8.configure(justify=LEFT)
        self.Checkbutton8.configure(text='''Yaw''')
        self.Checkbutton8.configure(variable=self.yaw_ctrl)

        self.Checkbutton9 = Checkbutton(self.Frame2)
        self.Checkbutton9.place(relx=0.53, rely=0.24, relheight=0.07
                , relwidth=0.33)
        self.Checkbutton9.configure(activebackground="#d9d9d9")
        self.Checkbutton9.configure(activeforeground="#000000")
        self.Checkbutton9.configure(background="#d9d9d9")
        self.Checkbutton9.configure(disabledforeground="#a3a3a3")
        self.Checkbutton9.configure(font=font16)
        self.Checkbutton9.configure(foreground="#000000")
        self.Checkbutton9.configure(highlightbackground="#d9d9d9")
        self.Checkbutton9.configure(highlightcolor="black")
        self.Checkbutton9.configure(justify=LEFT)
        self.Checkbutton9.configure(text='''Roll''')
        self.Checkbutton9.configure(variable=self.roll_ctrl)

        self.x_vel4 = Checkbutton(self.Frame2)
        self.x_vel4.place(relx=0.06, rely=0.31, relheight=0.1, relwidth=0.37)
        self.x_vel4.configure(activebackground="#d9d9d9")
        self.x_vel4.configure(activeforeground="#000000")
        self.x_vel4.configure(background="#d9d9d9")
        self.x_vel4.configure(disabledforeground="#a3a3a3")
        self.x_vel4.configure(font=font16)
        self.x_vel4.configure(foreground="#000000")
        self.x_vel4.configure(highlightbackground="#d9d9d9")
        self.x_vel4.configure(highlightcolor="black")
        self.x_vel4.configure(justify=LEFT)
        self.x_vel4.configure(text='''Altitude''')
        self.x_vel4.configure(variable=self.altitude_ctrl)

        self.Radiobutton2 = Radiobutton(self.Frame2)
        self.Radiobutton2.place(relx=0.12, rely=0.72, relheight=0.07
                , relwidth=0.24)
        self.Radiobutton2.configure(activebackground="#d9d9d9")
        self.Radiobutton2.configure(activeforeground="#000000")
        self.Radiobutton2.configure(background="#d9d9d9")
        self.Radiobutton2.configure(disabledforeground="#a3a3a3")
        self.Radiobutton2.configure(font=font16)
        self.Radiobutton2.configure(foreground="#000000")
        self.Radiobutton2.configure(highlightbackground="#d9d9d9")
        self.Radiobutton2.configure(highlightcolor="black")
        self.Radiobutton2.configure(justify=LEFT)
        self.Radiobutton2.configure(text='''0.2s''')
        self.Radiobutton2.configure(value="0.2")
        self.Radiobutton2.configure(variable = self.log_rate_ctrl)
        self.Radiobutton3 = Radiobutton(self.Frame2)
        self.Radiobutton3.place(relx=0.55, rely=0.63, relheight=0.07
                , relwidth=0.24)
        self.Radiobutton3.configure(activebackground="#d9d9d9")
        self.Radiobutton3.configure(activeforeground="#000000")
        self.Radiobutton3.configure(background="#d9d9d9")
        self.Radiobutton3.configure(disabledforeground="#a3a3a3")
        self.Radiobutton3.configure(font=font16)
        self.Radiobutton3.configure(foreground="#000000")
        self.Radiobutton3.configure(highlightbackground="#d9d9d9")
        self.Radiobutton3.configure(highlightcolor="black")
        self.Radiobutton3.configure(justify=LEFT)
        self.Radiobutton3.configure(text='''0.5s''')
        self.Radiobutton3.configure(value=".5")
        self.Radiobutton3.configure(variable = self.log_rate_ctrl)
        self.Radiobutton4 = Radiobutton(self.Frame2)
        self.Radiobutton4.place(relx=0.55, rely=0.71, relheight=0.07
                , relwidth=0.24)
        self.Radiobutton4.configure(activebackground="#d9d9d9")
        self.Radiobutton4.configure(activeforeground="#000000")
        self.Radiobutton4.configure(background="#d9d9d9")
        self.Radiobutton4.configure(disabledforeground="#a3a3a3")
        self.Radiobutton4.configure(font=font16)
        self.Radiobutton4.configure(foreground="#000000")
        self.Radiobutton4.configure(highlightbackground="#d9d9d9")
        self.Radiobutton4.configure(highlightcolor="black")
        self.Radiobutton4.configure(justify=LEFT)
        self.Radiobutton4.configure(text='''0.1s''')
        self.Radiobutton4.configure(value=".1")
        self.Radiobutton4.configure(variable = self.log_rate_ctrl)
        self.Radiobutton5 = Radiobutton(self.Frame2)
        self.Radiobutton5.place(relx=0.12, rely=0.63, relheight=0.07
                , relwidth=0.24)
        self.Radiobutton5.configure(activebackground="#d9d9d9")
        self.Radiobutton5.configure(activeforeground="#000000")
        self.Radiobutton5.configure(background="#d9d9d9")
        self.Radiobutton5.configure(disabledforeground="#a3a3a3")
        self.Radiobutton5.configure(font=font16)
        self.Radiobutton5.configure(foreground="#000000")
        self.Radiobutton5.configure(highlightbackground="#d9d9d9")
        self.Radiobutton5.configure(highlightcolor="black")
        self.Radiobutton5.configure(justify=LEFT)
        self.Radiobutton5.configure(text='''1.0s''')
        self.Radiobutton5.configure(value="1")
        self.Radiobutton5.configure(variable = self.log_rate_ctrl)
        self.Label1 = Label(self.Frame2)
        self.Label1.place(relx=0.2, rely=0.46, height=41, width=124)
        self.Label1.configure(activebackground="#f0f0f0f0f0f0")
        self.Label1.configure(activeforeground="#808080")
        self.Label1.configure(background="#808080")
        self.Label1.configure(disabledforeground="#a3a3a3")
        self.Label1.configure(font=font17)
        self.Label1.configure(foreground="#ffffff")
        self.Label1.configure(highlightbackground="#d9d9d9")
        self.Label1.configure(highlightcolor="black")
        self.Label1.configure(text='''Frequency''')
        self.Label1.configure(width=124)

        self.DeataFeed1 = Label( self.window)
        self.DeataFeed1.place(relx=0.01, rely=0.22, height=61, width=266)
        self.DeataFeed1.configure(activebackground="#f9f9f9")
        self.DeataFeed1.configure(activeforeground="black")
        self.DeataFeed1.configure(background="#008000")
        self.DeataFeed1.configure(disabledforeground="#a3a3a3")
        self.DeataFeed1.configure(font=font12)
        self.DeataFeed1.configure(foreground="#ffffff")
        self.DeataFeed1.configure(highlightbackground="#d9d9d9")
        self.DeataFeed1.configure(highlightcolor="black")
        self.DeataFeed1.configure(text='''Desired Drone Data''')
        self.DeataFeed1.configure(width=266)

        self.Apply_Changes_Button = Button( self.window)
        self.Apply_Changes_Button.place(relx=0.07, rely=0.73, height=24
                , width=91)
        self.Apply_Changes_Button.configure(activebackground="#d9d9d9")
        self.Apply_Changes_Button.configure(activeforeground="#000000")
        self.Apply_Changes_Button.configure(background="#d9d9d9")
        self.Apply_Changes_Button.configure(disabledforeground="#a3a3a3")
        self.Apply_Changes_Button.configure(foreground="#000000")
        self.Apply_Changes_Button.configure(highlightbackground="#d9d9d9")
        self.Apply_Changes_Button.configure(highlightcolor="black")
        self.Apply_Changes_Button.configure(pady="0")
        self.Apply_Changes_Button.configure(command= lambda : self.set_log_data(self.x_ctrl, self.y_ctrl, self.z_ctrl, self.yaw_ctrl, self.pitch_ctrl, self.roll_ctrl, self.altitude_ctrl,self.log_rate_ctrl))
        self.Apply_Changes_Button.configure(text='''Apply Changes''')

        self.BatteryLevel = Label(self.window)
        self.BatteryLevel.place(relx=0.74, rely=0.16, height=41, width=124)
        self.BatteryLevel.configure(background="#000000")
        self.BatteryLevel.configure(disabledforeground="#a3a3a3")
        self.BatteryLevel.configure(font=font16)
        self.BatteryLevel.configure(foreground="#ffffff")
        self.BatteryLevel.configure(text='''Battery Level''')
        self.BatteryLevel.configure(width=124)

        self.batVal = Label(self.window)
        self.batVal.place(relx=0.83, rely=0.16, height=41, width=84)
        self.batVal.configure(background="#c0c0c0")
        self.batVal.configure(borderwidth="10")
        self.batVal.configure(disabledforeground="#a3a3a3")
        self.batVal.configure(font=font14)
        self.batVal.configure(foreground="#ffffff")
        self.batVal.configure(highlightthickness="10")
        self.batVal.configure(padx="10")
        self.batVal.configure(pady="10")
        self.batVal.configure(text='''%''')
        self.batVal.configure(textvariable= self.batVal_ctrl)
        self.batVal.configure(width=84)

        self.window.protocol("WM_DELETE_WINDOW", self.shutdown)
        self.window.mainloop()
        

if __name__ == '__main__':
    multiprocessing.freeze_support()
    d = drone_app_controller()
    d.vp_start_gui()




