##############################################################################
# | communicator file |                                                      #
# Author(s): Afeefa Firdaus, Reuben Massaquoi                                #                                                  #
##############################################################################


import socket

"""the communicator class is a wrapper class for python sockets"""

class communicator(object):

    #private variables start w "_"
    _port = None
    _IP = None
    _socket = None
    _connection_type = None

    
    #@param:
        #arg1: an integer port number for the communication
        #arg2: a string IP address
        #arg3: an integer connection type.
            # 0 indicates UDP and 1 indicates TCP
    def __init__(self, port, IP, connection_type):
        self._port = port
        self._IP = IP
        self._socket = None
        self._connection_type = connection_type
        
        if self._connection_type == 0:
            self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #if bind:
         #   self._socket.bind((self._IP, self._port))
        
        #check connection type and instantiate socket as UDP or TCP.
        #if else ---
        ## talk back & forth // send and receive packets

    
     #takes a packet argument and sends that packet to the port based on connecton_type
    def send_packets(self, packet):
         #use a socket to send a packet
         #only focus on UDP for now.
         self._socket.sendto(bytes(packet,"utf-8"),(self._IP, self._port))

    
    #returns data from the other end of the communication port
    def recieve_packets(self,size = 1024):
        try:
 #           self._socket.bind((self._IP, self._port))
            return self._socket.recv(size)
        except:
            print("packet error")
            return None
        
         
         #try except blocks   
         
       # 0 & 1. 0 are UDP, 1 are TCP
        
        

        

#try to connect to the drone. If it doesnt work,
#then print a return statement.
#
##    
##try:
##    socket.socket().connect(DroneIP)
##    socket.socket().close()
##
##    except:
##        self.printRed()
##        print "Not online."
##



