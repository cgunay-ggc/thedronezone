AR Drone Controller Application for Windows
===========================================

Purpose
-------
The purpose of this project is to develop a windows based drone controller application for the Ar Parrot 2.0 drone quad-copter



License Agreement
-----------------

The source and product are licensed under the [Creative Commons Attribution-NonCommercial 4.0 international OSS license](https://creativecommons.org/licenses/by-nc/4.0/legalcode)

---



Original Developers
-------------------
1. Name: Reuben Boima Massaquoi
	Role: *Lead Programmer*
2. Name: Afeefa Firdaus
	Role: *Lead Documenter*
3. Name: Brian Bailey
	Role: *Lead Data-Modeler*

Repository: https://bitbucket.org/cgunay-ggc/thedronezone

Process Tool: http://itec-gunay.duckdns.org:8080/projects/DRON/summary

Slack Channel: https://ggc-dev.slack.com/messages/G6S2UQR7Z/files/F6XSZNXHD/

---

You are a Future Developer
=============================
The [doc](https://bitbucket.org/cgunay-ggc/thedronezone/src/0a91716a41153655827846b4f09e59910561441f/doc/?at=master) folder contains useful information for understanding the source.

Hardware requirments include: Windows OS and a TTX Tech wired analog controller for Sony ps1/ps2 in order to control the drone.

Tools and Dependencies include:

1. Python 3.6.2, 32-bit
2. Pygame version 1.9.3, 32-bit
3. Python tkinter/tcl (should already be included with latest python)


----

You are a User
==============

software instructions:

1. download the [dist.zip](https://bitbucket.org/cgunay-ggc/thedronezone/src/0a91716a41153655827846b4f09e59910561441f/dist.zip?at=master&fileviewer=file-view-default) file.

2. download the Ar Drone Controller Application User manual


