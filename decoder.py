##Decoder file
import struct
##################################################################################################
###### Decode NavData							    ######
##################################################################################################
### Description:
### It follows lousy code for abetter documentation! Later there will be lousy code because of laziness; I will correct it later....maybe.
### You will (normally) find the names of the official AR.drone SDK 2.0, some comments and the official data type of that value.
### A lot of entries are reversed engineered; for some, I have no idea what they are doing or what their meaning is.
### It would be nice if you could give me a hint if you have some further information.

#The following code is a modified fragment of the ps-drone api located here:https://github.com/reixd/ps-drone
#ORIGINAL AUTHORS LICENSE
#https://opensource.org/licenses/artistic-license-2.0

#The Following changes were made to this source:
#Replaced deprecated functions and outdated code
#Changed the global offset variable from an int to a list

#global offset variable
offsetND = [0]

##### Header ##################################################################
def decode_Header(data):
#Bit 00-07: FLY_MASK, VIDEO_MASK, VISION_MASK, CONTROL_MASK, ALTITUDE_MASK, USER_FEEDBACK_START, COMMAND_MASK, CAMERA_MASK
#Bit 08-15: TRAVELLING_MASK, USB_MASK, NAVDATA_DEMO_MASK, NAVDATA_BOOTSTRAP, MOTORS_MASK, COM_LOST_MASK, SOFTWARE_FAULT, VBAT_LOW
#Bit 16-23: USER_EL, TIMER_ELAPSED, MAGNETO_NEEDS_CALIB, ANGLES_OUT_OF_RANGE, WIND_MASK, ULTRASOUND_MASK, CUTOUT_MASK, PIC_VERSION_MASK
#Bit 24-31: ATCODEC_THREAD_ON, NAVDATA_THREAD_ON, VIDEO_THREAD_ON, ACQ_THREAD_ON, CTRL_WATCHDOG_MASK, ADC_WATCHDOG_MASK, COM_WATCHDOG_MASK, EMERGENCY_MASK
	stateBit = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	stateBit[ 0] = data[1]    &1	#  0: FLY MASK :					(0) ardrone is landed, (1) ardrone is flying
	stateBit[ 1] = data[1]>> 1&1	#  1: VIDEO MASK :					(0) video disable, (1) video enable
	stateBit[ 2] = data[1]>> 2&1	#  2: VISION MASK :					(0) vision disable, (1) vision enable
	stateBit[ 3] = data[1]>> 3&1	#  3: CONTROL ALGO :				(0) euler angles control, (1) angular speed control
	stateBit[ 4] = data[1]>> 4&1	#  4: ALTITUDE CONTROL ALGO :	 	(0) altitude control inactive (1) altitude control active
	stateBit[ 5] = data[1]>> 5&1	#  5: USER feedback : 				Start button state
	stateBit[ 6] = data[1]>> 6&1	#  6: Control command ACK : 		(0) None, (1) one received
	stateBit[ 7] = data[1]>> 7&1	#  7: CAMERA MASK : 				(0) camera not ready, (1) Camera ready
	stateBit[ 8] = data[1]>> 8&1	#  8: Travelling mask : 			(0) disable, (1) enable
	stateBit[ 9] = data[1]>> 9&1	#  9: USB key : 					(0) usb key not ready, (1) usb key ready
	stateBit[10] = data[1]>>10&1	# 10: Navdata demo : 				(0) All navdata, (1) only navdata demo
	stateBit[11] = data[1]>>11&1	# 11: Navdata bootstrap : 			(0) options sent in all or demo mode, (1) no navdata options sent
	stateBit[12] = data[1]>>12&1	# 12: Motors status : 				(0) Ok, (1) Motors problem
	stateBit[13] = data[1]>>13&1	# 13: Communication Lost : 			(0) Com is ok, (1) com problem
	stateBit[14] = data[1]>>14&1	# 14: Software fault detected - user should land as quick as possible (1)
	stateBit[15] = data[1]>>15&1	# 15: VBat low : 					(0) Ok, (1) too low
	stateBit[16] = data[1]>>16&1	# 16: User Emergency Landing :		(0) User EL is OFF, (1) User EL is ON
	stateBit[17] = data[1]>>17&1	# 17: Timer elapsed : 				(0) not elapsed, (1) elapsed
	stateBit[18] = data[1]>>18&1	# 18: Magnetometer calib state :	(0) Ok, no calibration needed, (1) not ok, calibration needed
	stateBit[19] = data[1]>>19&1	# 19: Angles :						(0) Ok,	(1) out of range
	stateBit[20] = data[1]>>20&1	# 20: WIND MASK:					(0) Ok, (1) Too much wind
	stateBit[21] = data[1]>>21&1	# 21: Ultrasonic sensor :			(0) Ok, (1) deaf
	stateBit[22] = data[1]>>22&1	# 22: Cutout system detection :		(0) Not detected, (1) detected
	stateBit[23] = data[1]>>23&1	# 23: PIC Version number OK :		(0) a bad version number, (1) version number is OK
	stateBit[24] = data[1]>>24&1	# 24: ATCodec thread ON : 			(0) thread OFF, (1) thread ON
	stateBit[25] = data[1]>>25&1	# 25: Navdata thread ON : 			(0) thread OFF, (1) thread ON
	stateBit[26] = data[1]>>26&1	# 26: Video thread ON : 			(0) thread OFF, (1) thread ON
	stateBit[27] = data[1]>>27&1	# 27: Acquisition thread ON : 		(0) thread OFF, (1) thread ON
	stateBit[28] = data[1]>>28&1	# 28: CTRL watchdog : 				(0) control is well scheduled, (1) delay in control execution (> 5ms)
	stateBit[29] = data[1]>>29&1	# 29: ADC Watchdog :				(0) uart2 is good, (1) delay in uart2 dsr (> 5ms)
	stateBit[30] = data[1]>>30&1	# 30: Communication Watchdog :		(0) Com is ok, (1) com problem
	stateBit[31] = data[1]>>31&1	# 31: Emergency landing : 			(0) no emergency, (1) emergency
	stateBit[32] = data[2]
	stateBit[33] = data[3]
	# Alternative code:
	#	for i in range (0,32,1):	arState[i]=data>>i&1
	return (stateBit)

##### ID = 0 ### "demo" #######################################################
def decode_ID0(packet):		# NAVDATA_DEMO_TAG
	dataset = struct.unpack_from("HHIIfffifffIffffffffffffIIffffffffffff", packet, 0)
	if dataset[1] != 148:		print("*** ERROR : Navdata-Demo-Options-Package (ID=0) has the wrong size !!!")
	demo=[[0,0,0,0,0,0,0,0,0,0,0,0],0,[0,0,0],0,[0,0,0],0,[0,0,0,0,0,0,0,0,0],[0,0,0],0,0,[0,0,0,0,0,0,0,0,0],[0,0,0]]
	demo[0][ 0] = dataset[2]>>15&1	# DEFAULT			(bool)
	demo[0][ 1] = dataset[2]>>16&1	# INIT				(bool)
	demo[0][ 2] = dataset[2]>>17&1	# LANDED			(bool)
	demo[0][ 3] = dataset[2]>>18&1	# FLYING			(bool)
	demo[0][ 4] = dataset[2]>>19&1	# HOVERING			(bool)  (Seems like landing)
	demo[0][ 5] = dataset[2]>>20&1	# TEST				(bool)
	demo[0][ 6] = dataset[2]>>21&1	# TRANS_TAKEOFF		(bool)
	demo[0][ 7] = dataset[2]>>22&1	# TRANS_GOFIX		(bool)
	demo[0][ 8] = dataset[2]>>23&1	# TRANS_LANDING		(bool)
	demo[0][ 9] = dataset[2]>>24&1	# TRANS_LOOPING		(bool)
	demo[0][10] = dataset[2]>>25&1	# TRANS_NO_VISION	(bool)
	demo[0][11] = dataset[2]>>26&1	# NUM_STATE			(bool)
	demo[1]		=dataset[3]			# vbat_flying_percentage	battery voltage (filtered) in percent	(uint32)
	demo[2][0]	=dataset[4]/1000.0	# theta						pitch in degrees						(float)
	demo[2][1]	=dataset[5]/1000.0	# phi						roll  in degrees						(float)
	demo[2][2]	=dataset[6]/1000.0	# psi						yaw   in degrees						(float)
	demo[3]		=dataset[7]/10.0	# altitude					altitude in centimetres					(int32)
	demo[4][0]	=dataset[8]			# vx						estimated speed in X in mm/s			(float)	
	demo[4][1]	=dataset[9]			# vy						estimated speed in Y in mm/s			(float)
	demo[4][2]	=dataset[10]		# vz						estimated speed in Z in mm/s			(float)
	demo[5]		=dataset[11]		# num_frames				streamed frame index 					(uint32) (Not used to integrate in video stage)
	for i in range (0,9,1):	demo[6][i]	= dataset[12+i]	# detection_camera_rot		Camera parameters compute by detection	(float matrix33)
	for i in range (0,3,1):	demo[7][i]	= dataset[21+i]	# detection_camera_trans	Deprecated ! Don't use !				(float vector31)
	demo[8]								= dataset[24]	# detection_tag_index		Deprecated ! Don't use !				(uint32)
	demo[9]								= dataset[25]	# detection_camera_type   	Type of tag								(uint32)
	for i in range (0,9,1):	demo[10][i]	= dataset[26+i]	# drone_camera_rot			Camera parameters computed by drone		(float matrix33)
	for i in range (0,3,1):	demo[11][i]	= dataset[35+i]	# drone_camera_trans		Deprecated ! Don't use !				(float vector31)
	return(demo)
##	
####### ID = 1 ### "time" #######################################################
def decode_ID1(packet):			#NAVDATA_TIME_TAG
        dataset = struct.unpack_from("HHI", packet, 0)
        if dataset[1] != 8:		 return("*** ERROR : navdata-time-Options-Package (ID=1) has the wrong size !!!")
        
        time=[0.0]
        # Value: 11 most significant bits represent the seconds, and the 21 least significant bits represent the microseconds.
        for i in range(0,21,1):		time[0] += ((dataset[2]>>i&1)*(2**i))		# Calculating the millisecond-part
        time[0] /= 1000000
        for i in range(21,32,1):	time[0] += (dataset[2]>>i&1)*(2**(i-21))	# Calculating second-part
        return(time)

####### ID = 2 ### "raw_measures" ################################################
def decode_ID2(packet):			#NAVDATA_RAW_MEASURES_TAG
        dataset = struct.unpack_from("HHHHHhhhhhIHHHHHHHHHHHHhh", packet, 0)
        if dataset[1] != 52:		print ("*** ERROR : navdata-raw_measures-Options-Package (ID=2) has the wrong size !!!")
        raw_measures = [[0,0,0],[0,0,0],[0,0],0,0,0,0,0,0,0,0,0,0,0,0,0]
        for i in range(0,3,1):	raw_measures[0][i] = dataset[2+i]	# raw_accs[xyz]			filtered accelerometer-datas [LSB]	(uint16)
        for i in range(0,3,1):	raw_measures[1][i] = dataset[5+i]	# raw_gyros[xyz]		filtered gyrometer-datas [LSB]		(int16)
        for i in range(0,2,1):	raw_measures[2][i] = dataset[8+i]	# raw_gyros_110[xy]		gyrometers  x/y 110 deg/s [LSB]		(int16)
        raw_measures[ 3] = dataset[10]		# vbat_raw				battery voltage raw (mV)			(uint)
        raw_measures[ 4] = dataset[11]		# us_debut_echo			[LSB]								(uint16)
        raw_measures[ 5] = dataset[12]		# us_fin_echo			[LSB]								(uint16)
        raw_measures[ 6] = dataset[13]		# us_association_echo	[LSB]								(uint16)
        raw_measures[ 7] = dataset[14]		# us_distance_echo		[LSB]								(uint16)
        raw_measures[ 8] = dataset[15]		# us_courbe_temps		[LSB]								(uint16)
        raw_measures[ 9] = dataset[16]		# us_courbe_valeur		[LSB]								(uint16)
        raw_measures[10] = dataset[17]		# us_courbe_ref			[LSB]								(uint16)
        raw_measures[11] = dataset[18]		# flag_echo_ini			[LSB]								(uint16)
        raw_measures[12] = dataset[19]		# nb_echo				[LSB]								(uint16)
        raw_measures[13] = dataset[21]		# sum_echo				juRef_st lower 16Bit, upper 16Bit=tags?	(uint32)
        raw_measures[14] = dataset[23]		# alt_temp_raw			in Milimeter	(just lower 16Bit)	(int32)
        raw_measures[15] = dataset[24]		# gradient				[LSB]								(int16)
        return(raw_measures)

####### ID = 3 ### "phys_measures" ##############################################
##def decode_ID3(packet):  		#NAVDATA_PHYS_MEASURES_TAG
##	dataset = struct.unpack_from("HHfHffffffIII", packet, 0)
##	if dataset[1] != 46:		print ("*** ERROR : navdata-phys_measures-Options-Package (ID=3) has the wrong size !!!")
##	phys_measures = [0,0,[0,0,0],[0,0,0],0,0,0]
##	phys_measures[0] = dataset[2]	#float32   accs_temp
##	phys_measures[1] = dataset[3]	#uint16    gyro_temp
##	phys_measures[4] = dataset[10]	#uint32    alim3V3              3.3volt alim [LSB]
##	phys_measures[5] = dataset[11]	#uint32    vrefEpson            ref volt Epson gyro [LSB]
##	phys_measures[6] = dataset[12]	#uint32    vrefIDG              ref volt IDG gyro [LSB]
##	dataset = struct.unpack_from(">HHfHffffffIII", packet, 0) 	#switch from little to big-endian
##	for i in range(0,3,1):	phys_measures[2][i] = dataset[4+i]	#float32   phys_accs[xyz] 
##	for i in range(0,3,1):	phys_measures[3][i] = dataset[7+i]	#float32   phys_gyros[xyz]
##	return(phys_measures)
##
####### ID = 4 ### "gyros_offsets" ##############################################
##def decode_ID4(packet):  		#NNAVDATA_GYROS_OFFSETS_TAG
##	dataset = struct.unpack_from("HHfff", packet, 0)
##	if dataset[1] != 16:		print ("*** ERROR : navdata-gyros_offsets-Options-Package (ID=4) has the wrong size !!!")
##	gyros_offsets = [0,0,0]
##	for i in range (0,3,1):		gyros_offsets[i]=dataset[i+2]	# offset_g[xyz]				in deg/s					(float)
##	return(gyros_offsets)
##
####### ID = 5 ### "euler_angles" ###############################################
##def decode_ID5(packet):			#NAVDATA_EULER_ANGLES_TAG
##	dataset = struct.unpack_from("HHff", packet, 0)
##	if dataset[1] != 12:		print ("*** ERROR : navdata-euler_angles-Options-Package (ID=5) has the wrong size !!!")
##	euler_angles = [0,0]
##	euler_angles[0] = dataset[2]	#float32   theta_a (head/back)
##	euler_angles[1] = dataset[3]	#float32   phi_a   (sides)
##	return(euler_angles)
##
####### ID = 6 ### "references" #################################################
##def decode_ID6(packet):			#NAVDATA_REFERENCES_TAG
##	dataset = struct.unpack_from("HHiiiiiiiiffffffIfffffI", packet, 0)
##	if dataset[1] != 88:		print ("*** ERROR : navdata-references-Options-Package (ID=6) has the wrong size !!!")
##	references = [[0,0,0],[0,0],[0,0,0],[0.0,0.0],[0.0,0.0],[0.0,0.0],0,[0.0,0.0,0.0,0.0,0.0,0]]
##	references[0][0] = dataset[2]		#ref_theta  	Theta_ref_embedded [milli-deg]	(int32)
##	references[0][1] = dataset[3]		#ref_phi		Phi_ref_embedded [milli-deg]	(int32)
##	references[0][2] = dataset[9]		#ref_psi		Psi_ref_embedded [milli-deg]	(int32)
##	references[1][0] = dataset[4]		#ref_theta_I	Theta_ref_int [milli-deg]		(int32)
##	references[1][1] = dataset[5]		#ref_phi_I		Phi_ref_int [milli-deg]			(int32)
##	references[2][0] = dataset[6]		#ref_pitch		Pitch_ref_embedded [milli-deg]	(int32)
##	references[2][1] = dataset[7]		#ref_roll		Roll_ref_embedded [milli-deg]	(int32)
##	references[2][2] = dataset[8]		#ref_yaw		Yaw_ref_embedded [milli-deg/s]	(int32)
##	references[3][0] = dataset[10]		#vx_ref			Vx_Ref_[mm/s]					(float)
##	references[3][1] = dataset[11]		#vy_ref			Vy_Ref_[mm/s]					(float)
##	references[4][0] = dataset[12]		#theta_mod		Theta_modele [radian]			(float)
##	references[4][1] = dataset[13]		#phi_mod		Phi_modele [radian]				(float)
##	references[5][0] = dataset[14]		#k_v_x											(float)
##	references[5][1] = dataset[15]		#k_v_y											(float)
##	references[6]    = dataset[16]		#k_mode											(uint32)
##	references[7][0] = dataset[17]		#ui_time										(float)
##	references[7][1] = dataset[18]		#ui_theta										(float)
##	references[7][2] = dataset[19]		#ui_phi											(float)
##	references[7][3] = dataset[20]		#ui_psi											(float)
##	references[7][4] = dataset[21]		#ui_psi_accuracy								(float)
##	references[7][5] = dataset[22]		#ui_seq											(int32)
##	return(references)
##
####### ID = 7 ### "trims" ######################################################
##def decode_ID7(packet):			#NAVDATA_TRIMS_TAG
##	dataset = struct.unpack_from("HHfff", packet, 0)
##	if dataset[1] != 16:		print ("*** ERROR : navdata-trims-Options-Package (ID=7) has the wrong size !!!")
##	trims = [0,0,0]
##	trims[0] = dataset[2]	#  angular_rates_trim									(float)
##	trims[1] = dataset[3]	#  euler_angles_trim_theta	[milli-deg]					(float)
##	trims[2] = dataset[4]	#  euler_angles_trim_phi	[milli-deg]					(float)
##	return(trims)
##
####### ID = 8 ### "rc_references" ##############################################
##def decode_ID8(packet):			#NAVDATA_RC_REFERENCES_TAG
##	dataset = struct.unpack_from("HHiiiii", packet, 0)
##	if dataset[1] != 24:		print ("*** ERROR : navdata-rc_references-Options-Package (ID=8) has the wrong size !!!")
##	rc_references = [0,0,0,0,0]
##	rc_references[0] = dataset[2]	#  rc_ref_pitch		Pitch_rc_embedded			(int32)
##	rc_references[1] = dataset[3]	#  rc_ref_roll		Roll_rc_embedded			(int32)
##	rc_references[2] = dataset[4]	#  rc_ref_yaw		Yaw_rc_embedded				(int32)
##	rc_references[3] = dataset[5]	#  rc_ref_gaz		Gaz_rc_embedded				(int32)
##	rc_references[4] = dataset[6]	#  rc_ref_ag		Ag_rc_embedded				(int32)
##	return(rc_references)
##
####### ID = 9 ### "pwm" ########################################################
def decode_ID9(packet):			#NAVDATA_PWM_TAG
	dataset = struct.unpack_from("HHBBBBBBBBffffiiifiiifHHHHff", packet, 0)
	if dataset[1] != 76 and dataset[1] != 92:   #92 since firmware 2.4.8 ?
		print ("*** ERROR : navdata-navdata_pwm-Options-Package (ID=9) has the wrong size !!!")
		#print "Soll: 76     Ist:",dataset[1]
	pwm = [[0,0,0,0],[0,0,0,0],0.0,0.0,0.0,0.0,[0,0,0],0.0,[0,0,0,0.0],[0,0,0,0],0.0,0.0]
	for i in range(0,4,1):	pwm[0][i] = dataset[2+i]	#  motor1/2/3/4		[Pulse-width mod]	(uint8)
	for i in range(0,4,1):	pwm[1][i] = dataset[6+i]	#  sat_motor1/2/3/4	[Pulse-width mod]	(uint8)
	pwm[2]    = dataset[10]			# gaz_feed_forward		[Pulse-width mod]	(float)
	pwm[3]    = dataset[11]			# gaz_altitud			[Pulse-width mod]	(float)
	pwm[4]    = dataset[12]			# altitude_integral		[mm/s]				(float)
	pwm[5]    = dataset[13]			# vz_ref				[mm/s]				(float)
	pwm[6][0] = dataset[14]			# u_pitch				[Pulse-width mod]	(int32)
	pwm[6][1] = dataset[15]			# u_roll				[Pulse-width mod]	(int32)
	pwm[6][2] = dataset[16]			# u_yaw					[Pulse-width mod]	(int32)
	pwm[7]    = dataset[17]			# yaw_u_I				[Pulse-width mod]	(float)
	pwm[8][0] = dataset[18]			# u_pitch_planif		[Pulse-width mod]	(int32)
	pwm[8][1] = dataset[19]			# u_roll_planif			[Pulse-width mod]	(int32)
	pwm[8][2] = dataset[20]			# u_yaw_planif			[Pulse-width mod]	(int32)
	pwm[8][3] = dataset[21]			# u_gaz_planif			[Pulse-width mod]	(float)
	for i in range(0,4,1):
		pwm[9][i] = dataset[22+i]	# current_motor1/2/3/4	[mA]				(uint16)
	pwm[10]   = dataset[26]			# altitude_prop			[Pulse-width mod]	(float)
	pwm[11]   = dataset[27]			# altitude_der			[Pulse-width mod]	(float)
	return(pwm)
##
####### ID = 10 ### "altitude" ###################################################
##def decode_ID10(packet):		#NAVDATA_ALTITUDE_TAG
##	dataset = struct.unpack_from("HHifiiffiiiIffI", packet, 0)
##	if dataset[1] != 56:		print( "*** ERROR : navdata-navdata_altitude-Options-Package (ID=10) has the wrong size !!!")
##	altitude = [0,0.0,0,0,0.0,0.0,[0,0,0],0,[0,0],0]
##	altitude[0] = dataset[2]			# altitude_vision	[mm]					(int32)
##	altitude[1] = dataset[3]			# altitude_vz		[mm/s]					(float)
##	altitude[2] = dataset[4]			# altitude_ref		[mm]					(int32)
##	altitude[3] = dataset[5]			# altitude_raw		[mm]					(int32)
##	altitude[4] = dataset[6]			# obs_accZ			Observer AccZ [m/s2]	(float)
##	altitude[5] = dataset[7]			# obs_alt			Observer altitude US [m](float)
##	for i in range (0,3,1):
##		altitude[6][i] = dataset[8+i]	# obs_x				3-Vector				(int32)
##	altitude[7] = dataset[11]			# obs_state			Observer state [-]		(uint32)
##	for i in range (0,2,1):
##		altitude[8][i] = dataset[12+i]	# est_vb			2-Vector				(float)
##	altitude[9] = dataset[14]			# est_state			Observer flight state 	(uint32)
##	return(altitude)
##
####### ID = 11 ### "vision_raw" #################################################
##def decode_ID11(packet):		#NAVDATA_VISION_RAW_TAG
##	dataset = struct.unpack_from("HHfff", packet, 0)
##	if dataset[1] != 16:	print ("*** ERROR : navdata-vision_raw-Options-Package (ID=11) has the wrong size !!!")
##	vision_raw = [0,0,0]
##	for i in range (0,3,1):		vision_raw[i] = dataset[2+i]	#  vision_tx_raw (xyz)				(float)
##	return(vision_raw)
##
####### ID = 12 ### "vision_of" #################################################
##def decode_ID12(packet):		#NAVDATA_VISION_OF_TAG
##	dataset = struct.unpack_from("HHffffffffff", packet, 0)
##	if dataset[1] != 44:	print ("*** ERROR : navdata-vision_of-Options-Package (ID=12) has the wrong size !!!")
##	vision_of = [[0.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0,0.0]]
##	for i in range (0,5,1):		vision_of[0][i] = dataset[2+i]	#  of_dx[5]							(float)
##	for i in range (0,5,1):		vision_of[1][i] = dataset[7+i]	#  of_dy[5]							(float)
##	return(vision_of)
##
####### ID = 13 ### "vision" #####################################################
##def decode_ID13(packet):		#NAVDATA_VISION_TAG
##	dataset = struct.unpack_from("HHIiffffifffiIffffffIIff", packet, 0)
##	if dataset[1] != 92:	print ("*** ERROR : navdata-vision-Options-Package (ID=13) has the wrong size !!!")
##	vision=[0,0,0.0,0.0,0.0,0.0,0,[0.0,0.0,0.0],0,0.0,[0.0,0.0,0.0],[0.0,0.0,0.0],0,0,[0.0,0.0]]
##	vision[0] = dataset[2]				# vision_state FIXME: What are the meanings of the tags ?
##	vision[1] = dataset[3]				# vision_misc							(int32)	
##	vision[2] = dataset[4]				# vision_phi_trim						(float)
##	vision[3] = dataset[5]				# vision_phi_ref_prop					(float)
##	vision[4] = dataset[6]				# vision_theta_trim						(float)
##	vision[5] = dataset[7]				# vision_theta_ref_prop					(float)
##	vision[6] = dataset[8]				# new_raw_picture						(int32)
##	for i in range (0,3,1):
##		vision[7][i] = dataset[9+i]		# theta/phi/psi_capture					(float)
##	vision[8] = dataset[12]				# altitude_capture						(int32)
##	for i in range (0,21,1):						# Calculating milisecond-part
##		vision[9] += ((dataset[13]>>i&1)*(2**i))
##	vision[9] /= 1000000
##	for i in range (21,32,1):						# Calculating second-part
##		vision[9] += (dataset[13]>>i&1)*(2**(i-21))	# time_capture			(float)
##	for i in range (0,3,1):
##		vision[10][i] = dataset[14+i]	#  velocities[xyz]						(float)
##	for i in range (0,3,1):
##		vision[11][i] = dataset[17+i]	#  delta_phi/theta/psi					(float)
##	vision[12] =    dataset[20]			# gold_defined							(uint32)
##	vision[13] =    dataset[21]			# gold_reset							(uint32)	
##	vision[14][0] = dataset[22]			# gold_x								(float)
##	vision[14][1] = dataset[23]			# gold_y								(float)
##	return(vision)
##
####### ID = 14 ### "vision_perf" ###############################################
##def decode_ID14(packet):		#NAVDATA_VISION_PERF_TAG
##	dataset = struct.unpack_from("HHffffffffffffffffffffffffff", packet, 0)
##	if dataset[1] != 108:		print( "*** ERROR : navdata-vision_of-Options-Package (ID=14) has the wrong size !!!")
##	vision_perf=[0.0,0.0,0.0,0.0,0.0,0.0,[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]]
##	vision_perf[0] = dataset[2]				# time_szo								(float)
##	vision_perf[1] = dataset[3]				# time_corners							(float)
##	vision_perf[2] = dataset[4]				# time_compute							(float)
##	vision_perf[3] = dataset[5]				# time_tracking							(float)
##	vision_perf[4] = dataset[6]				# time_trans							(float)
##	vision_perf[5] = dataset[7]				# time_update							(float)
##	for i in range (0,20,1):
##		vision_perf[6][i] = dataset[8+i]	# time_custom[20]						(float)
##	return(vision_perf)
##
####### ID = 15 ### "trackers_send" #############################################
##def decode_ID15(packet):  		#NAVDATA_TRACKERS_SEND_TAG
##	dataset = struct.unpack_from("HHiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii", packet, 0)
##	if dataset[1] != 364:		print ("*** ERROR : navdata-trackers_send-Options-Package (ID=15) has the wrong size !!!")
##	DEFAULT_NB_TRACKERS_WIDTH  = 6
##	DEFAULT_NB_TRACKERS_HEIGHT = 5
##	limit = DEFAULT_NB_TRACKERS_WIDTH*DEFAULT_NB_TRACKERS_HEIGHT
##	trackers_send = [[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],[[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0],[0,0]]]
##	for i in range (0, limit, 1):
##		trackers_send[0][i] = dataset[2+i]			#  locked[limit]				(int32)
##	for i in range (0, limit, 1):
##		trackers_send[1][i][0] = dataset[32+(i*2)]	#  point[x[limit],y[limit]]		(int32)
##		trackers_send[1][i][1] = dataset[33+(i*2)]
##	return(trackers_send)
##
####### ID = 16 ### "vision_detect" #############################################
##def decode_ID16(packet):  		#NAVDATA_VISION_DETECT_TAG
##	dataset = struct.unpack_from("HHIIIIIIIIIIIIIIIIIIIIIIIIIffffIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII", packet, offsetND)
##	if dataset[1] != 328:	print ("*** ERROR : navdata-vision_detect-Package (ID=16) has the wrong size !!!")
##	vision_detect = [0,[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0.0,0.0,0.0,0.0],[[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0]],[[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0]],[0,0,0,0]]
##	#Max marker detection in one picture: 4
##	vision_detect[0] = dataset[2]									 		# nb_detected						(uint32)
##	for i in range (0,4,1):		vision_detect[1][i] = dataset[3+i]			# type[4]							(uint32)
##	for i in range (0,4,1):		vision_detect[2][i] = dataset[7+i]			# xc[4]								(uint32)
##	for i in range (0,4,1):		vision_detect[3][i] = dataset[11+i]			# yc[4]								(uint32)
##	for i in range (0,4,1):		vision_detect[4][i] = dataset[15+i]			# width[4]							(uint32)
##	for i in range (0,4,1):		vision_detect[5][i] = dataset[19+i]			# height[4]							(uint32)
##	for i in range (0,4,1):		vision_detect[6][i] = dataset[23+i]			# dist[4]							(uint32)
##	for i in range (0,4,1):		vision_detect[7][i] = dataset[27+i]			# orientation_angle[4]				(float)
##	for i in range (0,4,1):
##		for j in range (0,9,1):	vision_detect[8][i][j] = dataset[31+i+j]	# rotation[4]						(float 3x3 matrix (11,12,13,21,...)
##	for i in range (0,4,1):
##		for j in range (0,3,1):	vision_detect[9][i][j] = dataset[67+i+j]	# rotation[4]						(float 3 vector)
##	for i in range (0,4,1):		vision_detect[10][i] = dataset[79+i]		# camera_source[4]					(uint32)
##	return(vision_detect)
##
####### ID = 17 ### "watchdog" ###################################################
##def decode_ID17(packet):  		#NAVDATA_WATCHDOG_TAG
##	dataset = struct.unpack_from("HHI", packet, offsetND)
##	if dataset[1] != 8:	print ("*** ERROR : navdata-watchdog-Package (ID=17) has the wrong size !!!")
##	watchdog = dataset[2]		# watchdog			Watchdog controll [-]				(uint32)
##	return(watchdog)
##
####### ID = 18 ### "adc_data_frame" #############################################
##def decode_ID18(packet):  		#NAVDATA_ADC_DATA_FRAME_TAG
##	dataset = struct.unpack_from("HHIBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB", packet, offsetND)
##	if dataset[1] != 40:	print ("*** ERROR : navdata-adc_data_frame-Package (ID=18) has the wrong size !!!")
##	adc_data_frame = [0,[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]
##	adc_data_frame[0] = dataset[2]									# version								(uint32)
##	for i in range (0,32,1):	adc_data_frame[1][i] = dataset[3+i]	# data_frame[32]						(uint8)
##	return(adc_data_frame)
##
####### ID = 19 ### "video_stream" ###############################################
##def decode_ID19(packet):		#NAVDATA_VIDEO_STREAM_TAG
##	dataset = struct.unpack_from("HHBIIIIfIIIiiiiiII", packet, offsetND)
##	if dataset[1] != 65:	print( "*** ERROR : navdata-video_stream-Package (ID=19) has the wrong size !!!")
##	video_stream = [0,0,0,0,0,0.0,0,0,0,[0,0,0,0,0],0,0]
##	video_stream[0] = dataset[2]	# quant   		quantizer reference used to encode [1:31]   				(uint8)
##	video_stream[1] = dataset[3]	# frame_size	frame size in bytes   										(uint32)
##	video_stream[2] = dataset[4]	# frame_number	frame index   												(uint32)
##	video_stream[3] = dataset[5]	# atcmd_ref_seq	atmcd ref sequence number   								(uint32)
##	video_stream[4] = dataset[6]	# atcmd_mean_ref_gap	mean time between two consecutive atcmd_ref (ms)	(uint32)
##	video_stream[5] = dataset[7]	# atcmd_var_ref_gap															(float)
##	video_stream[6] = dataset[8]	# atcmd_ref_quality		estimator of atcmd link quality   					(uint32)
##	#Drone 2.0:
##	video_stream[7] = dataset[9]	# out_bitrate			measured out throughput from the video tcp socket	(uint32)
##	video_stream[8] = dataset[10]	# desired_bitrate		last frame size generated by the video encoder		(uint32)
##	for i in range (0,5,1):		video_stream[9][i] = dataset[11+i]	# data		misc temporary data				(int32)
##	video_stream[10] = dataset[16]	# tcp_queue_level		queue usage											(uint32)
##	video_stream[11] = dataset[17]	# fifo_queue_level		queue usage											(uint32)
##	return(video_stream)
##
####### ID = 20 ### "games" ######################################################
##def decode_ID20(packet):  		#NAVDATA_GAMES_TAG
##	dataset = struct.unpack_from("HHII", packet, offsetND)
##	if dataset[1] != 12:	print ("*** ERROR : navdata-games-Package (ID=20) has the wrong size !!!")
##	games = [0,0]
##	games[0] = dataset[2]	# double_tap_counter 			   							(uint32)
##	games[1] = dataset[3]	# finish_line_counter										(uint32)
##	return(games)
##
####### ID = 21 ### "pressure_raw" ###############################################
##def decode_ID21(packet):  		#NAVDATA_PRESSURE_RAW_TAG
##	dataset = struct.unpack_from("HHihii", packet, offsetND)
##	if dataset[1] != 18:	print ("*** ERROR : navdata-pressure_raw-Package (ID=21) has the wrong size !!!")
##	pressure_raw = [0,0,0,0]
##	pressure_raw[0] = dataset[2]	# up 			   									(int32)
##	pressure_raw[1] = dataset[3]	# ut												(int16)
##	pressure_raw[2] = dataset[4]	# Temperature_meas 									(int32)
##	pressure_raw[3] = dataset[5]	# Pression_meas										(int32)
##	return(pressure_raw)
##
####### ID = 22 ### "magneto" ####################################################
##def decode_ID22(packet):		#NAVDATA_MAGNETO_TAG
##	dataset = struct.unpack_from("HHhhhffffffffffffBifff", packet, offsetND)
##	if dataset[1] != 83:	print ("*** ERROR : navdata-magneto-Package (ID=22) has the wrong size !!!")
##	magneto = [[0,0,0],[0.0,0.0,0.0],[0.0,0.0,0.0],[0.0,0.0,0.0],0.0,0.0,0.0,0,0,0.0,0.0,0.0]
##	for i in range (0,3,1):		magneto[0][i]=dataset[2+i]	# mx/my/mz											(int16)
##	for i in range (0,3,1):		magneto[1][i]=dataset[5+i]	# magneto_raw		magneto in the body frame [mG]	(vector float)
##	for i in range (0,3,1):		magneto[2][i]=dataset[8+i]	# magneto_rectified									(vector float)
##	for i in range (0,3,1):		magneto[3][i]=dataset[11+i]	# magneto_offset									(vector float)
##	magneto[ 4] = dataset[14]								# heading_unwrapped 								(float)
##	magneto[ 5] = dataset[15]								# heading_gyro_unwrapped							(float)
##	magneto[ 6] = dataset[16]								# heading_fusion_unwrapped 							(float)
##	magneto[ 7] = dataset[17]								# magneto_calibration_ok							(char)
##	magneto[ 8] = dataset[18]								# magneto_state 									(uint32)
##	magneto[ 9] = dataset[19]								# magneto_radius									(float)
##	magneto[10] = dataset[20]								# error_mean 										(float)
##	magneto[11] = dataset[21]								# error_var											(float)
##	return(magneto)
##
####### ID = 23 ### "wind_speed" ################################################
##def decode_ID23(packet):		#NAVDATA_WIND_TAG
##	dataset = struct.unpack_from("HHfffffffffffff", packet, offsetND)
##	if dataset[1] != 56 and dataset[1] != 64:
##		print ("*** ERROR : navdata-wind_speed-Package (ID=23) has the wrong size !!!")
##	wind_speed = [0.0,0.0,[0.0,0.0],[0.0,0.0,0.0,0.0,0.0,0.0],[0.0,0.0,0.0]]
##	wind_speed[0]    = dataset[2]							# wind_speed 			   					(float)
##	wind_speed[1]    = dataset[3]							# wind_angle								(float)
##	wind_speed[2][0] = dataset[4]							# wind_compensation_theta 					(float)
##	wind_speed[2][1] = dataset[5]							# wind_compensation_phi						(float)
##	for i in range (0,6,1):	wind_speed[3][i]=dataset[6+i]	# state_x[1-6]								(float)
##	for i in range (0,3,1):	wind_speed[4][i]=dataset[7+i]	# magneto_debug[1-3]						(float)
##	return(wind_speed)
##
####### ID = 24 ### "kalman_pressure" ###########################################
##def decode_ID24(packet):  		#NAVDATA_KALMAN_PRESSURE_TAG
##	dataset = struct.unpack_from("HHffffffffff?f?ff??", packet, offsetND)
##	if dataset[1] != 72:	print ("*** ERROR : navdata-wind_speed-Package (ID=24) has the wrong size !!!")
##	kalman_pressure = [0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0,0.0,False,0.0,0.0,False,False]
##	kalman_pressure[ 0] = dataset[2]	# offset_pressure 			   					(float)
##	kalman_pressure[ 1] = dataset[3]	# est_z											(float)
##	kalman_pressure[ 2] = dataset[4]	# est_zdot 										(float)
##	kalman_pressure[ 3] = dataset[5]	# est_bias_PWM 									(float)
##	kalman_pressure[ 4] = dataset[6]	# est_biais_pression							(float)
##	kalman_pressure[ 5] = dataset[7]	# offset_US 			   						(float)
##	kalman_pressure[ 6] = dataset[8]	# prediction_US									(float)
##	kalman_pressure[ 7] = dataset[9]	# cov_alt 										(float)
##	kalman_pressure[ 8] = dataset[10]	# cov_PWM										(float)
##	kalman_pressure[ 9] = dataset[11]	# cov_vitesse									(float)
##	kalman_pressure[10] = dataset[12]	# bool_effet_sol								(bool)
##	kalman_pressure[11] = dataset[13]	# somme_inno									(float)
##	kalman_pressure[12] = dataset[14]	# flag_rejet_US									(bool)
##	kalman_pressure[13] = dataset[15]	# u_multisinus									(float)
##	kalman_pressure[14] = dataset[16]	# gaz_altitude									(float)
##	kalman_pressure[15] = dataset[17]	# Flag_multisinus								(bool)
##	kalman_pressure[16] = dataset[18]	# Flag_multisinus_debut							(bool)
##	return(kalman_pressure)
##
####### ID = 25 ### "hdvideo_stream" ############################################
##def decode_ID25(packet):		#NAVDATA_HDVIDEO-TAG
##	dataset = struct.unpack_from("HHfffffff", packet, offsetND)
##	if dataset[1] != 32:	print ("*** ERROR : navdata-hdvideo_stream-Package (ID=25) has the wrong size !!!")
##	hdvideo_stream = [0.0,0.0,0.0,0.0,0.0,0.0,0.0]
##	hdvideo_stream[0] = dataset[2]	# hdvideo_state 			   					(float)
##	hdvideo_stream[1] = dataset[3]	# storage_fifo_nb_packets						(float)
##	hdvideo_stream[2] = dataset[4]	# storage_fifo_size 							(float)
##	hdvideo_stream[3] = dataset[5]	# usbkey_size 			USB key in kb (no key=0)(float)
##	hdvideo_stream[4] = dataset[6]	# usbkey_freespace		USB key in kb (no key=0)(float)
##	hdvideo_stream[5] = dataset[7]	# frame_number 			PaVE field of the frame starting to be encoded for the HD stream (float)
##	hdvideo_stream[6] = dataset[8]	# usbkey_remaining_time	[sec]					(float)
##	return(hdvideo_stream)
##
####### ID = 26 ### "wifi" ######################################################
##def decode_ID26(packet):		#NAVDATA_WIFI_TAG
##	dataset = struct.unpack_from("HHI", packet, offsetND)
##	if dataset[1] != 8:	print ("*** ERROR : navdata-wifi-Package (ID=26) has the wrong size !!!")
##	wifi = dataset[2]	# link_quality 			   								(uint32)
##	return(wifi)
##
####### ID = 27 ### "zimmu_3000" ################################################
##def decode_ID27(packet):  #NAVDATA_ZIMU_3000_TAG
##	dataset = struct.unpack_from("HHif", packet, offsetND)
##	if dataset[1] != 12 and dataset[1] != 216:		# 216 since firmware 2.4.8 ?
##		print( "*** ERROR : navdata-zimmu_3000-Package (ID=27) has the wrong size !!!")
##	zimmu_3000 = [0,0.0]
##	zimmu_3000[0] = dataset[2]	# vzimmuLSB 			   							(int32)
##	zimmu_3000[1] = dataset[3]	# vzfind 			   								(float)
##	return(zimmu_3000)

##### Footer ### "chksum" #####################################################
def decode_Footer(packet,allpacket):   ### Decode Checksum options-package ID=65535
        dataset = struct.unpack_from("HHI", packet)
        if dataset[1] != 8:	return "*** ERROR : Checksum-Options-Package (ID=65535) has the wrong size !!!"
        chksum = 	[0,False]
        chksum[0] = dataset[2]
        sum_, plen =	0, len(allpacket)-8
        try:
                for i in range(0,plen,1):
                        sum_ = sum_ +ord(chr(allpacket[i]))		# Slows down this Navdata-subprocess massivly
        except:
                return "decoding chksum error occured"
        if sum_ == chksum[0]:	chksum[1] = True
        return(chksum)


    
#The following code is derived from ps_drone api
def decode_navdata(nav_conn):
        
        #list of data that can be decoded
        packet_list =["demo","time","raw_measures","phys_measures","gyros_offsets","euler_angles","references","trims","rc_references","pwm","altitude","vision_raw","vision_of","vision","vision_perf","trackers_send","vision_detect","watchdog","adc_data_frame","video_stream","games","pressure_raw","magneto","wind_speed","kalman_pressure","hdvideo_stream","wifi","zimmu_3000","chksum","state"]
        #index of what's being decoded
        choice =	[False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,False,True]
        #what we want decoded
        decode_list = ["demo","time","chksum","pwm","raw_measures"]
        for i in range(len(packet_list)):
                if packet_list[i] in decode_list:
                        choice[i] = True
        running= True
        while running:
                packet = nav_conn.recv()
                navdata =	None
                if packet == "shutdown":
                        running = False
                elif packet:
                        
                        navdata =	{}
                        length =	len(packet)
                        dataset = 	struct.unpack_from("IIII", packet, 0)	# Reading (Header, State, Sequence, Vision)
                        navdata["state"] = decode_Header(dataset)
                        offsetND[0] = struct.calcsize("IIII")
                        #Demo-mode contains normally Option-Packages with ID=0 (_navdata_demo_t), ID=16 (seems empty) and ID=65535 (checksum)
                        #Full Mode contains
                        #offsetND[0]=0
                        try:
                                while offsetND[0] < length:
                                        dataset =  struct.unpack_from("HH", packet, offsetND[0])	# Reading (Header, Length)
                                        
                                        if dataset[0]== 0 and choice[ 0]:
                                                navdata["demo"]				= decode_ID0(packet[offsetND[0]:])
                                               
                                        if dataset[0]== 1 and choice[ 1]: navdata["time"] 				= decode_ID1(packet[offsetND[0]:])
        ##                                if dataset[0]== 2 and choice[ 2]: navdata["raw_measures"]		= decode_ID2(packet[offsetND[0]:])
        ##                                if dataset[0]== 3 and choice[ 3]: navdata["phys_measures"] 		= decode_ID3(packet[offsetND:])
        ##                                if dataset[0]== 4 and choice[ 4]: navdata["gyros_offsets"] 		= decode_ID4(packet[offsetND:])
        ##                                if dataset[0]== 5 and choice[ 5]: navdata["euler_angles"]		= decode_ID5(packet[offsetND:])
        ##                                if dataset[0]== 6 and choice[ 6]: navdata["references"] 		= decode_ID6(packet[offsetND:])
        ##                                if dataset[0]== 7 and choice[ 7]: navdata["trims"]				= decode_ID7(packet[offsetND:])
        ##                                if dataset[0]== 8 and choice[ 8]: navdata["rc_references"] 		= decode_ID8(packet[offsetND:])
        ##                                if dataset[0]== 9 and choice[ 9]: navdata["pwm"]				= decode_ID9(packet[offsetND[0]:])
        ##                                if dataset[0]==10 and choice[10]: navdata["altitude"]			= decode_ID10(packet[offsetND:])
        ##                                if dataset[0]==11 and choice[11]: navdata["vision_raw"] 		= decode_ID11(packet[offsetND:])
        ##                                if dataset[0]==12 and choice[12]: navdata["vision_of"] 			= decode_ID12(packet[offsetND:])
        ##                                if dataset[0]==13 and choice[13]: navdata["vision"]				= decode_ID13(packet[offsetND:])
        ##                                if dataset[0]==14 and choice[14]: navdata["vision_perf"]		= decode_ID14(packet[offsetND:])
        ##                                if dataset[0]==15 and choice[15]: navdata["trackers_send"] 		= decode_ID15(packet[offsetND:])
        ##                                if dataset[0]==16 and choice[16]: navdata["vision_detect"]		= decode_ID16(packet[offsetND:])
        ##                                if dataset[0]==17 and choice[17]: navdata["watchdog"] 			= decode_ID17(packet[offsetND:])
        ##                                if dataset[0]==18 and choice[18]: navdata["adc_data_frame"] 	= decode_ID18(packet[offsetND:])
        ##                                if dataset[0]==19 and choice[19]: navdata["video_stream"] 		= decode_ID19(packet[offsetND:])
        ##                                if dataset[0]==20 and choice[20]: navdata["games"]				= decode_ID20(packet[offsetND:])
        ##                                if dataset[0]==21 and choice[21]: navdata["pressure_raw"]		= decode_ID21(packet[offsetND:])
        ##                                if dataset[0]==22 and choice[22]: navdata["magneto"]			= decode_ID22(packet[offsetND:])
        ##                                if dataset[0]==23 and choice[23]: navdata["wind_speed"] 		= decode_ID23(packet[offsetND:])
        ##                                if dataset[0]==24 and choice[24]: navdata["kalman_pressure"]	= decode_ID24(packet[offsetND:])
        ##                                if dataset[0]==25 and choice[25]: navdata["hdvideo_stream"]		= decode_ID25(packet[offsetND:])
        ##                                if dataset[0]==26 and choice[26]: navdata["wifi"]				= decode_ID26(packet[offsetND:])
        ##                                if dataset[0]==27 and choice[27]: navdata["zimmu_3000"]			= decode_ID27(packet[offsetND:])
                                        if dataset[0]==65535 and choice[28]:
                                                try:
                                                        navdata["chksum"]			= decode_Footer(packet[offsetND[0]:],packet)
                                                except:
                                                        nav_conn.send("could not decode footer")
                                                
                                        offsetND[0] = offsetND[0]+ dataset[1]
                        except:
                                navdata = "Could not decode"
                        nav_conn.send(navdata)
 


                            
                    
                    
            
            
            
            
